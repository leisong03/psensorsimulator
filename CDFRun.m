allomega=[0.001,0.005,0.010];
marks={'r-d','k-o','b-*'};
legends={'\omega=1ms','\omega=5ms','\omega=10ms'};
cdfx=cell(length(allomega),1);
cdff=cell(length(allomega),1);
for RunRound=1:length(allomega)
    omega=allomega(RunRound);
    CumulatedError=SimObjFunc(omega);
   [ff,xx]=ecdf(CumulatedError)
   cdfx{RunRound}=xx;
   cdff{RunRound}=ff
end
figure();
clf();
hold on;
for RunRound=1:length(allomega)
    plot(cdfx{RunRound},cdff{RunRound},marks{RunRound});
end
lagend(legends);

