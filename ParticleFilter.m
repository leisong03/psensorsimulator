function [MLEXYObjEst,Particle,VelocitySample,isFollow]=ParticleFilter(Particle,ThisXYObjEst,Residule,NumParticleLimit,VelocitySample,TimeInterval);
%Parameter
% particle: particle for this object, which is represented by a cell n x m. n is the ParticleDepth,
%	    m is the the particle number
% ThisXYObjEst: a mat where location is stored column by column
[SortedResidule,SortedIdx]=sort(Residule); 
NumConservation=min(NumParticleLimit,length(Residule));
SortedIdx=SortedIdx(1: NumConservation);
ThisXYObjEst=ThisXYObjEst(SortedIdx,:);
NumNewLoc=size(ThisXYObjEst,1);
if NumNewLoc==0
    isFollow=0;
    MLEXYObjEst=Particle{end,1};
    return;
end
isFollow=1;
ThisXYObjEstCell=mat2cell(ThisXYObjEst,ones(NumNewLoc,1),[2]);
NumParticle=size(Particle,2);
[ix iy]=meshgrid([1 : NumParticle],[1 : NumNewLoc]);
idxCombine=[ix(:),iy(:)];
ParticleDepth=size(Particle,1); 
BurstedParticle=cell(ParticleDepth+1,NumNewLoc*NumParticle);

for i=1:size(idxCombine,1)
    oldIdx=idxCombine(i,1);
    newIdx=idxCombine(i,2);
    BurstedParticle(:,i)=[Particle(:,oldIdx);ThisXYObjEstCell(newIdx)];
end
%Particle sorting code here
VelOfParticle=zeros(size(BurstedParticle,2),1);
ProbOfParticle= zeros(size(BurstedParticle,2),1);
for i=1:size(BurstedParticle,2);
    ThisLoc=BurstedParticle{end,i};
    LastLoc=BurstedParticle{end-1,i};
    ThisVel=norm(ThisLoc-LastLoc)/TimeInterval;
    VelOfParticle(i)=ThisVel;
end

VelocitySample=[VelocitySample;VelOfParticle(find(VelOfParticle<5))];
if length(VelocitySample)<20;
    ProbOfParticle=VelOfParticle<5;
else
    MeanVel=mean(VelocitySample);
    StdVel=std(VelocitySample);
    VelocitySample=VelocitySample(end-19 : end);
    ProbOfParticle=normpdf(VelOfParticle,MeanVel,StdVel);
end

[Val,SortedIdx]=sort(ProbOfParticle,'descend');
BurstedParticle=BurstedParticle(:,SortedIdx);
Particle=BurstedParticle(2:end,:);
ParticleReserved=min(size(Particle,2),NumParticleLimit);
Particle=Particle(:,1 : ParticleReserved);
MLEXYObjEst=Particle{end,1};
end
