function [isMatched, ArcIdx, PatchToGo] = MatchPatch(ArcSet, BPSidx, IsIn, PatchSet, WhichPatch,PatchCenter, PSensorRadius,XYPSensor,PatchGraph)
isMatched = 0;

if (norm(PatchCenter(WhichPatch,:) - XYPSensor(BPSidx,:)) < PSensorRadius) ~= ( IsIn == 1 )
	isMatched = 0;
	ArcIdx = 0;
	PatchToGo = 0;
	return;
end
ThisPatch = PatchSet{WhichPatch};
idx = find(ThisPatch(:,1) == BPSidx);
isMatched = ~isempty(idx)
if isMatched == 0
	ArcIdx = 0;
	PatchToGo = 0;
	return;
end
ArcIdx = ThisPatch(idx,2);
PatchToGo = 0;
PossiblePatch = find(full(PatchGraph(WhichPatch,:) | PatchGraph(:,WhichPatch)')>0);
for i = 1 : length(PossiblePatch)
	PatchSet{PossiblePatch(i)}
	BPSidx
	ArcIdx
	connected = ismember([BPSidx,ArcIdx],PatchSet{PossiblePatch(i)},'rows');
	if connected == 1
		PatchToGo = PossiblePatch(i);
	end
end
end
