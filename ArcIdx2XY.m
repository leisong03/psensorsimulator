function [X,Y] = ArcIdx2XY(PSIdx, ArcIdx, ArcSet, XYPSensor, PSensorRadius)
X = [0 0 0];
Y = [0 0 0];
Angle = ArcSet{PSIdx}(ArcIdx,:);
X(1) = PSensorRadius*cos(Angle(1)) + XYPSensor(PSIdx,1);
X(2) = PSensorRadius*cos(Angle(2)) + XYPSensor(PSIdx,1);
Y(1) = PSensorRadius*sin(Angle(1)) + XYPSensor(PSIdx,2);
Y(2) = PSensorRadius*sin(Angle(2)) + XYPSensor(PSIdx,2);
if Angle(1) > Angle (2) 
    MiddleAngle = wrapTo2Pi((Angle(1)+ 2*pi + Angle(2))/2);
else 
    MiddleAngle =  (Angle(1) + Angle (2))/2;
end
X(3) = PSensorRadius*cos(MiddleAngle) + XYPSensor(PSIdx,1);
Y(3) = PSensorRadius*sin(MiddleAngle) + XYPSensor(PSIdx,2);
end
