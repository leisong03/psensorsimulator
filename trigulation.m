function [XY,FLAG,RELRES]=trigulation(XYRef,RangeToRef)
X=XYRef(:,1);
Y=XYRef(:,2);
D=RangeToRef;
A=[X(2:end)-X(1),Y(2:end)-Y(1)];
if nnz(A(:,1))==0 || nnz(A(:,2))==0 ;
    FLAG=-10;
    XY=[0,0];
    RELRES=1;
    return;
end
B=(D(1).^2-D(2:end).^2+X(2:end).^2-X(1).^2+Y(2:end).^2-Y(1).^2)./2;
[XY,FLAG,RELRES] =lsqr(A,B);
end
