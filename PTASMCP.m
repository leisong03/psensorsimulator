function [ Cnt,Color ] = PTASMCP( AdjMat )
%PTASMCP(AdjacentMatrix) giva a approximate solution to the MCP problem 
%  AdjMat: Adjecent matrix
%  Cnt: Number of set in partition 
%  Color: Color of each vertex
Cnt = 0;
Color = zeros(size(AdjMat,1),1);
while nnz(AdjMat) > 0  
    RetCell = maximalCliques(AdjMat);
    ThisSubMat = RetCell{1}; 
    Cnt = Cnt + 1;
    Color(ThisSubMat) = Cnt;
    AdjMat (ThisSubMat,:) = 0;
    AdjMat (:,ThisSubMat) = 0;
end
UnColoredVertex = nnz(~Color);
Color(Color == 0) =  Cnt+1 : Cnt+nnz(~Color) ;
Cnt = Cnt + UnColoredVertex;
end
