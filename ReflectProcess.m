function [NewXYObj,tdir]=ReflectProcess(XYObj,XYRegion,tdir)
NewXYObj=XYObj;

idx=find(XYObj(:,1)<0); % x lower than 0
NewXYObj(idx,1)=-XYObj(idx,1);
tdir(idx)=pi-tdir(idx);

idx=find(XYObj(:,1)>XYRegion(1)); % x greater than XRegion
NewXYObj(idx,1)=XYRegion(1)*2-XYObj(idx,1);
tdir(idx)=pi-tdir(idx);

idx=find(XYObj(:,2)<0); % y lower than 0
NewXYObj(idx,2)=-XYObj(idx,2);
tdir(idx)=-tdir(idx)+2*pi;
idx=find(XYObj(:,2)>XYRegion(2)); % y greater than YRegion
NewXYObj(idx,2)=XYRegion(2)*2-XYObj(idx,2);
tdir(idx)=-tdir(idx)+2*pi;
tdir=wrapTo2Pi(tdir);
end
