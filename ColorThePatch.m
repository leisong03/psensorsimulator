function ColorThePatch(handle,PatchSet,PatchesColorMat,ObjColor,BandColor,PatchCenter,ArcSet,XYPSensor,PSensorRadius) 
ColorfulPatchIdx = find(sum(PatchesColorMat) > 0);
ColorfulPatchIdx 
figure(handle);
for i = ColorfulPatchIdx
    ColorInThisPatch = find(PatchesColorMat(:,i)>0);
    %ColorInThisPatch = PatchesColorMat(ColorInThisPatch,i);
    [ ProfileX,ProfileY ] = PatchProfile(i, PatchSet, ArcSet, XYPSensor, PSensorRadius);
    AngleOfProfile = zeros(length(ProfileX),1);
    for j = 1 : length(AngleOfProfile)
	[AngleOfProfile(j), hello] = cart2pol(ProfileX(j) - PatchCenter(i,1),ProfileY(j) - PatchCenter(i,2));
    end
    AngleOfProfile = wrapTo2Pi(AngleOfProfile);
    n = length(ColorInThisPatch)
    if n == 1
	patch(ProfileX,ProfileY,ObjColor{ColorInThisPatch(1)});
	continue;
    end
    for j = 1 : n
	AngleRange = [(j-1)*2*pi/(n),j*2*pi/(n)]
	ProfileFallin = find(AngleOfProfile >= AngleRange(1) & AngleOfProfile <= AngleRange(2));
	PieceProfileX = [ProfileX(ProfileFallin),PatchCenter(i,1)];
	PieceProfileY = [ProfileY(ProfileFallin),PatchCenter(i,2)];
	ColorInThisPatch;
	patch(PieceProfileX,PieceProfileY,ObjColor{ColorInThisPatch(j)});
    end
end
end
