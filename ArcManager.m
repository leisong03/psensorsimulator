function [Arc,OverlappedCount,ArcFrom] = ArcManager(IntersectionSet,IntersectionFrom,MaxNumIntersection)
SortedIntersection = sort(IntersectionSet(:));
ShiftedIntersection = circshift(SortedIntersection,[-1,0]);
Arc = [SortedIntersection ShiftedIntersection]; 
DeltaArc = Arc(:,1) - Arc(:,2);
Arc = Arc( abs(DeltaArc) > 0.01,: ); 
ArcFrom = zeros(size(Arc, 1),MaxNumIntersection); 
OverlappedCount = zeros(size(Arc,1),1);
for i = 1 : size(Arc,1)-1
    MiddleAngle = mean(Arc(i,:));
    if Arc(i,1) < Arc(i,2)
	for j = 1 : size(IntersectionSet,1)
	    if IntersectionSet(j,1)<IntersectionSet(j,2)
		if MiddleAngle> IntersectionSet(j,1) && MiddleAngle< IntersectionSet(j,2)
		    OverlappedCount(i) = OverlappedCount(i)+ 1;
		    ArcFrom(i,OverlappedCount(i)) = IntersectionFrom(j);
		end
	    else
		if MiddleAngle < IntersectionSet(j,2) || (MiddleAngle > IntersectionSet(j,1) && MiddleAngle <2*pi)
		    OverlappedCount(i) = OverlappedCount(i)+ 1;
		    ArcFrom(i,OverlappedCount(i)) = IntersectionFrom(j);
		end
	    end
	end
    end
end
i = size(Arc,1);
for j = 1 : size(IntersectionSet,1)
    if IntersectionSet(j,1) > IntersectionSet(j,2) && IntersectionSet(j,1) <= Arc(i,1) && IntersectionSet(j,2) >= Arc(i,2)
	OverlappedCount(i) = OverlappedCount(i)+ 1;
	ArcFrom(i,OverlappedCount(i)) = IntersectionFrom(j);
    end
end
