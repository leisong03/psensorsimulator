function UpdateMove(h,InMove,OutMove,XYPSensor,PSensorRadius)
figure(h);
OutMove;
InMove;
for i = 1:size(InMove,1)
    FromIdx=InMove(i,1);
    ToIdx=InMove(i,2);
    fromXY = XYPSensor(FromIdx,:);
    toXY = XYPSensor(ToIdx,:);
    diffXY=toXY-fromXY;
    [Theta,R]=cart2pol(diffXY(1),diffXY(2));
    [offsetX,offsetY]=pol2cart(Theta,PSensorRadius);
    [offsetX2,offsetY2]=pol2cart(Theta,.5);
    lh=quiver(toXY(1)-offsetX,toXY(2)-offsetY,offsetX2,offsetY2);
    set(lh,'linewidth',2);
end

for i = 1:size(OutMove,1)
    FromIdx=OutMove(i,1);
    ToIdx=OutMove(i,2);
    fromXY = XYPSensor(FromIdx,:);
    toXY = XYPSensor(ToIdx,:);
    diffXY=toXY-fromXY;
    [Theta,R]=cart2pol(diffXY(1),diffXY(2));
    [offsetX,offsetY]=pol2cart(Theta,PSensorRadius);
    [offsetX2,offsetY2]=pol2cart(Theta,.5);
    lh=quiver(fromXY(1)+offsetX,fromXY(2)+offsetY,offsetX2,offsetY2);
    set(lh,'linewidth',2);
end
end
