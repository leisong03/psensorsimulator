close all;
%%%%%%%%%%%%%%%%%%%%%%%%%% Configure moduel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
UsingPresenceSensor=1;
UpdatePatchGraph = 1;
UsingRangeSensor = 0;
UsingMicrowaveSensor = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%% Objects and Region %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NumObj = 5; % number of objects
NumParticleLimit =  20; % number of maximum particle
PSensorRadius = sqrt(2); % sensing radius of presence sensor
MSensorRadius = 1; % sensing radius of Motion sensor
XYRegion = [10,10]; % size of the reogion
XYMotion = [8,8]; % size of the reogion

AgentVel=1;
delta=0.01;
T=[0:delta:10];
XYObj=repmat(XYMotion,NumObj,1).* rand(NumObj,2); %size of the
AgentVel=repmat(AgentVel,NumObj,1);
MaxVelocity=max(AgentVel);

%%%%%%%%%%%%%%%%%%%%%%%% using presence sensor%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial state
if UsingPresenceSensor
	NumPSensor = 20; % number of presence sensor
	MaxNumIntersection = 10; %Number of Intersection on one censing circle
	MaxArcInPatch = 20; %Number of arc in on epatch
	MaxNumPatch = 500; % max number of patch
	%distributed uniformlly
	%[tx ty] = meshgrid([0 :2*sqrt(2) : XYRegion(1)],[0 :2*sqrt(2) :XYRegion(2)]);
	[tx ty] = meshgrid([0 :sqrt(2) : XYRegion(1)],[0 :sqrt(2) :XYRegion(2)]); %
	%XYPSensor = repmat(XYRegion, NumPSensor,1).*rand(NumPSensor,2);
	XYPSensor = [tx(:) ty(:)];
	NumPSensor = size(XYPSensor,1) % number of presence sensor
	PSensorMat = AdjacentMatrix(XYPSensor,PSensorRadius); %adjacent matrix
	PSensorShrinkSegment = cell(NumPSensor,1);
	PSensorExpandSegment = cell(NumPSensor,2);
	%XYPSensor=repmat(XYRegion,NumPSensor,1).* rand(NumPSensor,2); %size of the
	PSensorState = zeros(1,NumPSensor); % state of Presence sensor
	PSensorStateHistory = zeros(NumPSensor,10); % state of Presence sensor in last short while
	LastPSensorState = zeros(1,NumPSensor);
	ArcColor = {'r-','g-','b-','c-','m-','y-','k-'};
	ObjColor = {'r','g','b','c','m','k','y'};
	BandColor = {'y','m','g','b','r','k','y'};
	NumSamplePerArc = 20;
	NoView = 1;
	DrawPatchDAG = 0;
	MaxNumParticle = 200;
	MaxNumTarget = 5;
	ShiftWindowLength = 10;
	Particle = cell(MaxNumParticle,1);
	ArcOfParticle = cell(MaxNumParticle,ShiftWindowLength);
	UpdateTime = zeros(1,ShiftWindowLength);
	NumParticles = 0;
	NumTracesInParticle = zeros(MaxNumParticle,1);
	CumulatedNumTracesInParticle = zeros(MaxNumParticle,20);

	FeasibleRegion = cell(MaxNumParticle, 1); %feasible region
	NeedToInit = 1;
	ShiftWindowIdx = 0;
	%BiographHandler  = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%% using microwave sensor $%%%%%%%%%%%%%%%%%%%%%%%%
if UsingMicrowaveSensor
	NumMSensor=10;
	XYMSensor=repmat(XYRegion,NumMSensor,1).* rand(NumMSensor,2); %size of the
	MSensorInWave=ones(1,NumMSensor)*1000;
	MSensorOutWave=ones(1,NumMSensor)*1000;
	DangerMat=sparse(length([0:5:XYRegion(1)]),length([0:5:XYRegion(2)]));
	DangerMat=rand(length([0:5:XYRegion(1)]),length([0:5:XYRegion(2)]))>.7;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Using Range sensor %%%%%%%%%%%%%%%%%%%%%
if UsingRangeSensor
	%NumRangeSensor=10;		    %number of range sensor
	RangeSensorRadius=5;	    % sensing radius of range sensor
	LengthAfterShock=omega*330;	    % length of aftershock
	[tx ty]=meshgrid([-5 :2 : XYRegion(1)+5],[-5 :2 :XYRegion(2)+5]); % Sensor is deployed uniformlly on X-Y
	XYRangeSensor = [tx(:),ty(:)];
	NumRangeSensor = size(XYRangeSensor,1);
	LastLocated=0;		    % to determine if locate success in last time
	LastLocationEst = zeros(NumObj,2);	%Estimated location in last time
	VelocitySample = cell(NumObj,1);  %Historical velocity estimation
	IntervalCount=5;		    % Ranging is carried out every 5 tic-toc
	deltaRange=delta*IntervalCount; % Time interval of two ranging
	RealRange=sparse(NumRangeSensor,NumObj); % Store the real range between emitter to  sensor
	TotalParticle=cell(NumObj,1);   % Save all particle
	MaxStepInTrack=100;		    % number of steps in track
	MLETracks	=   cell(MaxStepInTrack,NumObj);  %Track with maximum likelihood
	RealTracks	=   cell(MaxStepInTrack,NumObj);  %RealTracks
	ValidTrackStep =0;		    %number of valid step in each track
	CumulatedError = ones(1,length(T))*inf;
	CumulatedErrorCount = 0;
	FollowState = ones(1,NumObj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if UsingPresenceSensor
	PatchHandler = figure();
	PatchGraphHandler = figure();
	TraceHandler = figure();
	AllPatchHandler = figure();
	%BiographHandler = figure();
end
if UsingRangeSensor
	h3=figure();
	movegui(h3,'northwest');
	h4=figure();
	movegui(h4,'north');
	h5=figure();
	movegui(h5,'west');
	h6=figure();
	movegui(h6,'center');
end

ttmemo=[];
PrintCount=0;
%%%%%%%%%%%%%%%%%%%Generating neighboring graph of neighboring graph%%%%
if UsingPresenceSensor && UpdatePatchGraph
	[V, U] = meshgrid([1 : NumPSensor],[1 : NumPSensor]);
	pair = [V(:), U(:)]; %Combination of every two Psensor
	PairDist= zeros(NumPSensor,NumPSensor);
	%reshape the dist matrix
	PairDist = reshape(sqrt(sum((XYPSensor(pair(:,1), 1)-XYPSensor(pair(:,2), 1)).^2+(XYPSensor(pair(:,1), 2)-XYPSensor(pair(:,2), 2)).^2,2)), NumPSensor, NumPSensor);
	NeighbourPair = find(pair(:,1) ~= pair(:,2) & PairDist(:) < 2*PSensorRadius-0.001);
	NeighbourGraph = sparse(NumPSensor,NumPSensor);
	NeighbourGraph(NeighbourPair) = PairDist(NeighbourPair);
	
	%%%%%%%%%%%%%%%%%%%%generating arc set%%%%%%%%%%%%%%%%%%%
	IntersectionSet = cell(NumPSensor,1);  %Start angle and end angle of every Intersection
	IntersectionFrom = cell(NumPSensor,1);  % Intersection given by which BPS
	ArcSet = cell(NumPSensor, 1); % Intersection overlapped to form ArcSet
	ArcFrom = cell(NumPSensor, 1); % Arc is overlapped by Intersection given by which
	ArcOverlappedCount = cell(NumPSensor, 1); %Arc overlapped by how many intersection
	PatchSet = cell(MaxNumPatch, 1); %all patches
	PatchIdx = zeros(MaxNumPatch, MaxNumIntersection); %identity of each patch
	PatchCenter = zeros(MaxNumPatch, 2); %center location of each patch
	NumPatch = 0;
	[U,V] =find(tril(NeighbourGraph)>0); %Go through every edge in neighbour graph to form the intersection set
	% calculate intersection between every two sensing circle of BPS
	for edge = [U, V]'
		[intx,inty]=circcirc(XYPSensor(edge(1),1),XYPSensor(edge(1),2),PSensorRadius,XYPSensor(edge(2),1),XYPSensor(edge(2),2),PSensorRadius);
		for j = 1:2;
			%two angle of each intersection
			[alpha,r] = cart2pol(intx(1) - XYPSensor(edge(j),1),inty(1) - XYPSensor(edge(j),2));
			alpha = wrapTo2Pi(alpha);
			[beta,r] = cart2pol(intx(2) - XYPSensor(edge(j),1),inty(2) - XYPSensor(edge(j),2));
			beta = wrapTo2Pi(beta);
			AngleDiff = max(alpha, beta) - min(alpha, beta);
			if AngleDiff < pi && AngleDiff > 0
				FirstAngle = min(alpha, beta);
				SecondAngle = max(alpha, beta);
			else
				FirstAngle = max(alpha, beta);
				SecondAngle = min(alpha, beta);
			end
			%Intersection angle of each sensing circle
			IntersectionSet{edge(j)}(end+1,:) = [FirstAngle,SecondAngle];
			IntersectionFrom{edge(j)}(end+1) = edge(3-j);
		end
		%plot(intx,inty,'ro','markers',10);
	end
	
	%Form the arc set  and patch set
	for  i = 1 : NumPSensor
		% find arc for each PSensor
		% ArcOverlappedCount is the number intersection overlapped on each arc
		% ArcFrom{i}(j,:) is the set of idx of psensor which covering jth arc in ith BPS
		[ArcSet{i},ArcOverlappedCount{i},ArcFrom{i}] = ArcManager(IntersectionSet{i}, IntersectionFrom{i}, MaxNumIntersection);
		NumArc = size (ArcSet{i}, 1)
		for j = 1 : NumArc
			InPatchIdx = [ArcFrom{i}(j,:)];
			InPatchIdx(find(InPatchIdx == 0,1, 'first')) = i; %replace first zero into psensor ID
			InPatchIdx = sort(InPatchIdx); %Sort the index
			%put zero back
			InPatchIdx = [InPatchIdx(find (InPatchIdx>0)),InPatchIdx(find (InPatchIdx ==0))];
			OutPatchIdx = sort([ ArcFrom{i}(j,:)] );
			OutPatchIdx = [OutPatchIdx(find (OutPatchIdx>0)),OutPatchIdx(find (OutPatchIdx ==0))];
			k = 1;
			%Put current in/outPatchIdx into the bucket
			while sum(InPatchIdx ~= PatchIdx(k,:),2) && k <= NumPatch
				k = k+1;
			end
			if k > NumPatch
				NumPatch = NumPatch + 1;
				PatchIdx(NumPatch,:) = InPatchIdx;
				PatchSet{NumPatch}(1,:) = [i,j]; %[idx_of_psensor, idx_of_arc]
			else
				PatchSet{k}(end+1,:)=[i,j];
			end
			k = 1;
			while sum(OutPatchIdx ~= PatchIdx(k,:),2) && k <= NumPatch
				k = k +1;
			end
			if k > NumPatch
				NumPatch = NumPatch + 1;
				PatchIdx(NumPatch,:) = OutPatchIdx;
				PatchSet{NumPatch}(1,:) = [i,j];
			else
				PatchSet{k}(end+1,:)=[i,j];
			end
		end
	end
	
	%trim the '0 0 0 0 ...' patchidx
	zElement = find( sum(PatchIdx,2) == 0);
	PatchSet(zElement) = [];
	PatchIdx(zElement,:) = [];
	PatchCenter(zElement,:) = [];
	NumPatch  = MaxNumPatch - nnz(zElement)
	
	%PatchesColorMat = zeros(NumPatch )
	% find profile for each patch
	for i = 1 : NumPatch
		NumArc = size(PatchSet{i},1);
		vx = [];
		vy = [];
		for j = 1 : NumArc
			PSIdx = PatchSet{i}(j,1);
			ArcIdx = PatchSet{i}(j,2);
			[thisx, thisy] = ArcIdx2XY(PSIdx, ArcIdx, ArcSet, XYPSensor, PSensorRadius);
			vx = [vx, thisx];
			vy = [vy, thisy];
		end
		PatchCenter(i,:) = [mean(vx),mean(vy)];
	end
	PatchGraph = sparse(NumPatch,NumPatch);
	for i = 1 : NumPatch
		for j = 1 : NumPatch
			PatchGraph(i,j) = i~=j && all(ismember(PatchIdx(i,:),PatchIdx(j,:))) && nnz(PatchIdx(i,:)) == nnz(PatchIdx(j,:))-1
		end
	end
	PatchSize = zeros(1,NumPatch);
	for i = 1 : NumPatch
		[ProfileX,ProfileY] = PatchProfile(i, PatchSet, ArcSet, XYPSensor, PSensorRadius);
		PatchSize(i) = polyarea(ProfileX,ProfileY);
	end
	PatchSize
end
if UsingPresenceSensor
	PatchesColorMat = zeros(NumObj, NumPatch);
end

%Run Simulation
LastPSensorState = zeros(1,NumPSensor);
PatchOccupied = zeros(1,NumPatch);
LastPatchOccupied = zeros(1,NumPatch);
count = 1;
UpdatedCount = 0;

TrackX = zeros(length(T),NumObj)
TrackY = zeros(length(T),NumObj)
EstTrackX = zeros(length(T),NumObj)
EstTrackY = zeros(length(T),NumObj)
CDFError = zeros(length(T),1)
for t=T;
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Generate track of object %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if mod(count,1000) == 1 % every 5 seconds, change the direction of agent
		tdir=2*pi*rand(NumObj,1);
	end
	count = count + 1;
	if count == 200 
		break
	end
	[DeltaX, DeltaY]=pol2cart(tdir,AgentVel*delta);
	XYOffset=[DeltaX, DeltaY];
	XYObj=XYObj+XYOffset;
	[XYObj,tdir]=ReflectProcess(XYObj,XYMotion,tdir);
	for i = 1 : NumObj
		TrackX(count - 1,i) = XYObj(i,1);
		TrackY(count - 1,i) = XYObj(i,2);
	end
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Simulation Respoence of Presence sensor %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if UsingPresenceSensor
		%calculate state of PSensor
		for k = 1 : NumPSensor
			PSensorState(k) = 0; % Initially all sensor is off
			for j = 1:NumObj
				if norm(XYObj(j,:) - XYPSensor(k,:)) < PSensorRadius;
					PSensorState(k) = 1; % the state of this BPS is on
					continue;
				end
			end
		end
		if PSensorState == LastPSensorState
			continue;
		end
		UpdatedCount = UpdatedCount + 1;
		time = t;
		PSensorState;
		PSensorStateHistory = [PSensorState' PSensorStateHistory(:,1:end-1)]; % A shifting window
		PatchOccupied = zeros(1,NumPatch);
		for i = 1 : NumPatch
			nzidx = find (PatchIdx(i,:) >0);
			nzidx = PatchIdx(i,nzidx);
			if nnz(PSensorState(nzidx)) == length(nzidx)
				PatchOccupied(i) = 1;
			end
		end
		[MLECount MLEPatch] = UpperMostPatch(PatchOccupied,PatchGraph,PSensorState,PatchIdx)
		for i = 1 : NumObj
			closeto = 0 ;
			distance = inf;
			for j = 1 : MLECount
				if norm( PatchCenter(MLEPatch(j),:) - XYObj(i,:)) < distance
					distance = norm( PatchCenter(MLEPatch(j),:) - XYObj(i,:));
					closeto = j;
				end
			end
			PatchesColorMat(i,MLEPatch(closeto)) = 1;
			EstTrackX(UpdatedCount,i) = PatchCenter(MLEPatch(closeto),1);+.1*sqrt(PatchSize(MLEPatch(closeto))).*rand(1);
			EstTrackY(UpdatedCount,i) = PatchCenter(MLEPatch(closeto),2);+.1*sqrt(PatchSize(MLEPatch(closeto))).*rand(1);;
			CDFError(UpdatedCount) = norm([EstTrackX(UpdatedCount,i),EstTrackY(UpdatedCount,i)]-XYObj(i,:));
		end
		
		if NeedToInit
			disp('guess init position');
			% MLE as the first patch
			NeedToInit = 0;
			NumParticles = 2;
			Particle{1} = cell(ShiftWindowLength, length(MLEPatch));
			for i = 1 : length(MLEPatch)
				Particle{1}{i,1} = [MLEPatch(i)];
			end
			NumTracesInParticle(1) = MLECount;
			%Random as the second patch
			OnPatch = find(PatchOccupied > 0);
			SubPatchGraph = PatchGraph(OnPatch, OnPatch);
			InWhichPatch = [];
			for i = 1 : NumObj
				IsMatch = 0;
				for j = 1 : NumPatch
					[ProfileX,ProfileY] = PatchProfile(j, PatchSet, ArcSet, XYPSensor, PSensorRadius);
					[isIn isOn ] = inpolygon(XYObj(i,1),XYObj(i,2),ProfileX, ProfileY);
					if isIn
						IsMatch = 1;
						break;
					end
				end
				if IsMatch
					InWhichPatch = [InWhichPatch, j];
				end
			end
			Particle{2} = cell(ShiftWindowLength,length(InWhichPatch));
			NumTracesInParticle(2) = NumObj;

			for i = 1 : length(InWhichPatch)
				Particle{2}{i,1} = [InWhichPatch(i)];
			end
			ShiftWindowIdx = 1;
			CumulatedNumTracesInParticle(:,ShiftWindowIdx) = NumTracesInParticle;

			ArcOfParticle{1,1} = [0,0];
			ArcOfParticle{2,1} = [0,0];
			UpdateTime(ShiftWindowIdx) = time;
		else
			StateDiff = PSensorState - LastPSensorState;
			ChangeBPS = find(StateDiff ~= 0);
			disp(['Changed BPSs are ',num2str(ChangeBPS)]);
			IdxPatchOccupied = find(PatchOccupied > 0);
			IdxLastPatchOccupied = find(LastPatchOccupied > 0);
			ShiftWindowIdx = ShiftWindowIdx + 1;
			UpdateTime(ShiftWindowIdx) = time;
			for i = ChangeBPS
				for j = 1 : NumParticles
					for k = 1 : size(Particle{j},1)
						FeasibleRegion = Particle{j}{k,ShiftWindowIdx-1};
						for q = FeasibleRegion
							ID = [i j k q];

							[isMatched, ArcIdx, PatchToGo] = MatchPatch(ArcSet,i,StateDiff(i) ~= 1,PatchSet, q,PatchCenter,PSensorRadius,XYPSensor,PatchGraph);
							if isMatched
								[isMatched, ArcIdx, PatchToGo]
								Particle{NumParticles+1} = Particle{j};
								NumTracesInParticle(NumParticles +1) = NumTracesInParticle(j)
								NumParticles = NumParticles + 1;
								Particle{NumParticles}{k,ShiftWindowIdx} = PatchToGo;
								ArcOfParticle(NumParticles,:) = ArcOfParticle(j,:);
								ArcOfParticle{NumParticles,ShiftWindowIdx} = [i,ArcIdx];
								disp('Matched');
							end
						end
					end
				end
			end
			NumParticles
			Particle
			DeltaTime = UpdateTime(ShiftWindowIdx) - UpdateTime(ShiftWindowIdx - 1);
			HasUpdated = zeros(1,NumParticles);
			for i = 1 : NumParticles
				for j = 1 : NumTracesInParticle(i)
					if ~isempty(Particle{i}{j,ShiftWindowIdx})
						HasUpdated(i) = 1;
					end
				end
				if HasUpdated(i)
					%extend
					for j = 1 : NumTracesInParticle(i)
						[i j ShiftWindowIdx]
						if isempty(Particle{i}{j,ShiftWindowIdx})
							FeasibleRegion = Particle{i}{j,ShiftWindowIdx-1};
							AddedRegion = [];
							for k = FeasibleRegion
								NeighborPatch = find(full(PatchGraph(k,:) | PatchGraph(:,k)')>0);
								for q = NeighborPatch
									if ismember(q,FeasibleRegion) || ismember(q,AddedRegion)
										continue;
									end
									if norm(PatchCenter(q,:) - PatchCenter(k,:)) < DeltaTime*MaxVelocity && PatchOccupied(q)
										AddedRegion = [AddedRegion q];
									end
									AddedRegion = [AddedRegion q];
									break;
								end
							end
							FeasibleRegion = unique([FeasibleRegion,AddedRegion]);
							Particle{i}{j,ShiftWindowIdx} = FeasibleRegion;
						end
					end
				end
			end
			Particle= Particle(find( HasUpdated > 0));
			NumParticles = nnz(HasUpdated);
			NumTracesInParticle = NumTracesInParticle(find( HasUpdated > 0));
			ArcOfParticle = ArcOfParticle(find( HasUpdated > 0),:);
			if ShiftWindowIdx > ShiftWindowLength
				for i = 1 : NumParticles
					Particle{i} = Particle{i}(:,end-ShiftWindowLength+1:end);
				end
			end
		end
	end
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Range sesnor %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if UsingRangeSensor
		if mod(i,IntervalCount) ~= 0
			disp('skip');
			continue;
		end
		disp('Ok');
		for j = 1 : NumRangeSensor
			for k=1:NumObj
				RealRange(j,k)=norm(XYRangeSensor(j,:)-XYObj(k,:));
			end
		end
		ValidTrackStep=ValidTrackStep+1;
		for k = 1 : NumObj
			RealTracks(ValidTrackStep,k)={XYObj(k,:)};
		end
		idx = find(full(RealRange)>RangeSensorRadius); % Range lost when exceed range sensor
		RealRange(idx) = 0;
		
		RealRangeNumMat = full(RealRange)>0;
		ObRange = sparse(NumRangeSensor,NumObj);
		ObRange = AnonymousProcess(RealRange,LengthAfterShock);% Considering aftershock effect
		EstError=zeros(1,NumObj);
		MeanVel=zeros(NumObj,1);
		StdVel=zeros(NumObj,1);
		if LastLocated == 0 %If last location is not success, i.e., the historical information is not available
			GuessedRangeNumMat=RealRangeNumMat;
			XYObjEst = ExpliciteLocate(RealRange,XYRangeSensor,NumObj);
			LastLocationEst=XYObjEst;
			for k=1 : NumObj
				TotalParticle{k} = {XYObjEst(k,:);XYObjEst(k,:)};
				MLETracks(ValidTrackStep,k)={XYObj(k,:)};
			end
			LastLocated=1;
			FollowState=ones(1,NumObj);
		else % otherwise
			[XYObjEstCell,STATE,Residule,GuessedRangeNumMat]=AnonymousLocate(ObRange,XYRangeSensor,NumObj,LastLocationEst,MaxVelocity,deltaRange);
			for k=1 : NumObj
				[XYObjEstMLE,TotalParticle{k},VelocitySample{k},isFollow]=ParticleFilter(TotalParticle{k},XYObjEstCell{k},Residule{k},NumParticleLimit,VelocitySample{k},deltaRange);
				if isFollow==0
					disp(['particle ',num2str(k),' lost']);
					FollowState(k)=0;
				end
				XYObjEst(k,:) = XYObjEstMLE;
				EstError(k)=norm(XYObjEstMLE-XYObj(k,:));
				MLETracks(ValidTrackStep,k)={XYObjEst(k,:)};
			end
			for i = 1 : NumObj
				MeanVel(i)=mean(VelocitySample{k});
				StdVel(i)=std(VelocitySample{k});
			end
			LastLocationEst=XYObjEst;
		end
		for k = 1 : NumObj
		end
		disp('RealRangeNum Observed GuessedRangeNum RangeLoss');
		sumofsum=[sum(RealRangeNumMat)',sum(full(ObRange)>0)', sum(GuessedRangeNumMat)',sum(RealRangeNumMat)'-sum(GuessedRangeNumMat)']
		CumulatedError(CumulatedErrorCount+1:CumulatedErrorCount+length(EstError))=EstError;
		CumulatedErrorCount=CumulatedErrorCount+length(EstError);
		if ValidTrackStep>MaxStepInTrack
			%trim is over-length
			RealTracks=RealTracks(2:end,:);
			MLETracks=MLETracks(2:end,:);
			ValidTrackStep=MaxStepInTrack;
		end
	end
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rendering  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if UsingPresenceSensor
		for i = 1 : NumParticles
			figure();
			hold on;
			theta = linspace(0,2*pi,40)';
			circ = [cos(theta) sin(theta)].*PSensorRadius;	
			ArcOfThisParticle = ArcOfParticle{i,ShiftWindowIdx};
			for j= 1 : NumPSensor
				%if isempty(ArcSet{i})
				shiftCirc=circ+repmat(XYPSensor(j,:),size(circ,1),1);
				plot (shiftCirc(:,1),shiftCirc(:,2),'k-');
				%end				
			end
			
			for j = find(PatchOccupied > 0)
				[ProfileX,ProfileY] = PatchProfile(j, PatchSet, ArcSet, XYPSensor, PSensorRadius);
				patch(ProfileX, ProfileY,[200 200 200]./255);
			end
			for j = 1 : NumTracesInParticle(i)
				FeasibleRegion = Particle{i}{j,ShiftWindowIdx};
				DrawThis = 1;
				if nnz(ArcOfThisParticle)
					for k = FeasibleRegion
						ThisPatch = PatchSet{k};
						if ismember(ArcOfThisParticle,ThisPatch,'rows')
							DrawThis = 0;
							StartTheta = ArcSet{ArcOfThisParticle(1)}(ArcOfThisParticle(2),1); EndTheta = ArcSet{ArcOfThisParticle(1)}(ArcOfThisParticle(2),2);
							if EndTheta < StartTheta
								EndTheta = EndTheta + 2*pi;
							end
							ThisTheta = linspace(StartTheta, EndTheta, NumSamplePerArc)';
							ThisArc = [cos(ThisTheta) sin(ThisTheta)].*PSensorRadius;
							shiftArc = ThisArc+repmat(XYPSensor(ArcOfThisParticle(1),:),NumSamplePerArc,1);
							%ColorIdx = ArcOverlappedCount{i}(j)+1;
							plot (shiftArc(:,1),shiftArc(:,2),BandColor{j});
							
						end
						
					end
				end
% 				if ShiftWindowIdx ~= 1
% 					OldFeasibleRegion = Particle{i}{j,ShiftWindowIdx-1}
% 					if OldFeasibleRegion == FeasibleRegion
% 						PossiblePatch = find(full(PatchGraph(FeasibleRegion,:) | PatchGraph(:,FeasibleRegion)')>0);
% 						FeasibleRegion = [FeasibleRegion PossiblePatch];
% 					end
% 				end
				if ~DrawThis
					continue;
				end

				for k = FeasibleRegion
					[ProfileX,ProfileY] = PatchProfile(k, PatchSet, ArcSet, XYPSensor, PSensorRadius);
					patch(ProfileX, ProfileY, BandColor{j});
				end
				%plot(TrackX(1:count -1,i),TrackY(1:count -1,i),ObjColor{i});
			end
			
			
			xlabel('x(m)');
			ylabel('y(m)');
			axis equal;
			sdf('pdf');
			set(gca,'box','on');
			saveas(gcf,[num2str(count-1),'-',num2str(i),'.pdf'],'pdf');
			close();
		end
		figure(PatchGraphHandler); % Plot patch distribution
		clf();
		hold on;
		ThisIdx = find(PatchOccupied > 0);
		plot(XYPSensor(:,1),XYPSensor(:,2),'gd');% plot '*' at center of every BPS
		plot(PatchCenter(:,1),PatchCenter(:,2),'rd');
		gplot(PatchGraph(ThisIdx,ThisIdx),PatchCenter(ThisIdx,:));
		for i = 1 : NumPatch
			if PatchOccupied(i) > 0
				text(PatchCenter(i,1)+0.05,PatchCenter(i,2)+0.05,num2str(i));
			end
		end
		xlabel('x(m)');
		ylabel('y(m)');
		axis equal;
		sdf('pdf');
		set(gca,'box','on');
		saveas(gcf,'PatchGraph','pdf');
		
		figure(TraceHandler);
		hold on;
		clf();
		for i = 1 : NumObj
			plot(TrackX(1:count -1,i),TrackY(1:count -1,i),ObjColor{i});
			plot(EstTrackX(1 : UpdatedCount,i),EstTrackY(1: UpdatedCount ,i),[ObjColor{i},'--']);
		end
		sdf('pdf');
		
		
		figure(PatchHandler);
		%clf();
		hold on;
		theta = linspace(0,2*pi,40)';
		circ = [cos(theta) sin(theta)].*PSensorRadius;
		
		for i = find(PatchOccupied > 0 )
			[ProfileX,ProfileY] = PatchProfile(i, PatchSet, ArcSet, XYPSensor, PSensorRadius);
			patch(ProfileX, ProfileY, [162 181 205]./255);
		end
		for i = MLEPatch
			closeto = 0;
			distance = inf;
			for j = 1 : NumObj
				if norm(PatchCenter(i,:) - XYObj(j,:)) < distance
					closeto = j;
					distance = norm(PatchCenter(i,:) - XYObj(j,:));
				end
			end
			[ProfileX,ProfileY] = PatchProfile(i, PatchSet, ArcSet, XYPSensor, PSensorRadius);
			patch(ProfileX, ProfileY,BandColor{closeto});
		end
		
		for i = 1 : NumObj
			plot(TrackX(1:count -1,i),TrackY(1:count -1,i),ObjColor{i});
			%plot(EstTrackX(1 : UpdatedCount,i),EstTrackY(1: UpdatedCount ,i),[ObjColor{i},'--']);
		end
		for i= 1 : NumPSensor
			%if isempty(ArcSet{i})
			shiftCirc=circ+repmat(XYPSensor(i,:),size(circ,1),1);
			 plot (shiftCirc(:,1),shiftCirc(:,2),'k-');
			%end
			text(XYPSensor(i,1)+0.005,XYPSensor(i,2),['P',num2str(i)],'color','r');

		end
		
		for i =1 : NumPSensor
			NumArc = size ( ArcSet{i}, 1 );
			for j = 1 : NumArc
				StartTheta = ArcSet{i}(j,1); EndTheta = ArcSet{i}(j,2);
				if EndTheta < StartTheta
					EndTheta = EndTheta + 2*pi;
				end
				MiddleTheta = (StartTheta + EndTheta)/2;
				ThisTheta = linspace(StartTheta, EndTheta, NumSamplePerArc)';
				ThisArc = [cos(ThisTheta) sin(ThisTheta)].*PSensorRadius;
				shiftArc = ThisArc+repmat(XYPSensor(i,:),NumSamplePerArc,1);
				ColorIdx = ArcOverlappedCount{i}(j)+1;
				%plot (shiftArc(:,1),shiftArc(:,2),ArcColor{ColorIdx});
				textxy = [cos(MiddleTheta) sin(MiddleTheta)].*PSensorRadius + XYPSensor(i,:);
				ThisFrom = ArcFrom{i}(j,:);
			end
		end
		
		for i = 1 : NumPatch
			NumArc = size(PatchSet{i},1);
			vx = [];
			vy = [];
			for j = 1 : NumArc
				PSIdx = PatchSet{i}(j,1);
				ArcIdx = PatchSet{i}(j,2);
				[thisx, thisy] = ArcIdx2XY(PSIdx, ArcIdx, ArcSet, XYPSensor, PSensorRadius);
				vx = [vx, thisx];
				vy = [vy, thisy];
			end
			idx = PatchIdx(i,find(PatchIdx(i,:) > 0));
			meanx = mean(vx);
			meany = mean(vy);
			Patchlabel = num2str(i);
			text(PatchCenter(i,1)+0.05,PatchCenter(i,2)+0.05,num2str(i));

		end
		
		for i = 1 : NumObj
			%plot(XYObj(i,1),XYObj(i,2),[ObjColor{i},'*'],'MarkerSize',15);
		end
		xlabel('x(m)');
		ylabel('y(m)');
		axis equal;
		sdf('pdf');
		set(gca,'box','on');
		saveas(gcf,'ArcGraph','pdf');
		if (t~=0 & DrawPatchDAG == 1)
			close(bgfig);
			DrawPatchDAG = 0;
		end
		OnPatchSet = find( PatchOccupied > 0);
		if nnz(PatchGraph(OnPatchSet,OnPatchSet)) > 0
			BioG = biograph(PatchGraph(OnPatchSet,OnPatchSet));
			for  i = 1 : length(OnPatchSet)
				BioG.nodes(i).ID = num2str(OnPatchSet(i));
				BioG.nodes(i).FontSize = 16;
			end
			for  i =  MLEPatch
				CurrenIdx = find( OnPatchSet == i)
				BioG.nodes(CurrenIdx).Color = [220 20 60]./255;
			end
			view(BioG);
			set(0, 'ShowHiddenHandles', 'on')
			bgfig = gcf;
			saveas(gcf,'PatchDAG','pdf');
			DrawPatchDAG = 1;
		end
	end
	
	if UsingRangeSensor
		figure(h3);
		%alwaysontodf(h3);
		clf();
		hold on;
		axis equal;
		axis ([0 XYRegion(1) 0 XYRegion(2)]);
		plot(XYRangeSensor(:,1),XYRangeSensor(:,2),'kd','markers',12,'linewidth',2);
		UpdateXYObj(h3,XYObj,XYRegion,10,2);
		DrawXYObj(h3,NumObj,XYObjEst,'ro',12,2);
		IdxMaxError=find( EstError==max(EstError),1,'first');
		TotalParticle{IdxMaxError};
		AllSpot=cell2mat(TotalParticle{IdxMaxError});
		AllSpotLastLine=AllSpot(end,:);
		%plot(AllSpotLastLine(1:2:end),AllSpotLastLine(2:2:end),'ro','markers',12,'linewidth',3);
		legend('Receiver','Real obj','Est obj');
		set(gca,'FontSize',15);
		%DrawRange(h3,XYObj,XYRangeSensor,RealRangeNumMat-GuessedRangeNumMat);
		
		figure (h4);
		hold on;
		%alwaysontop(h4);
		subplot(4,1,1)
		hold on;
		idx=find(FollowState);
		bar(idx,EstError(idx));
		idx=find(~FollowState);
		if ~isempty(idx)
			bar(idx,EstError(idx),'r');
		end
		title('locating error');
		xlabel('Identity of target');
		ylabel('Location error(m)');
		subplot(4,1,2)
		bar(sumofsum(:,3));
		title('Number of feasible range');
		xlabel('Identity of target');
		ylabel('Number');
		subplot(4,1,3)
		errorbar([1:NumObj],MeanVel,StdVel);
		set(gca,'FontSize',15);
		title('Esimated velocity');
		xlabel('Identity of target');
		ylabel('Velocity (m/s)');
		%pause();
		
		figure (h5);
		%alwaysontop(h5);
		clf();
		hold on;
		RealVelocity=zeros(ValidTrackStep-1,NumObj);
		MLEVelocity=zeros(ValidTrackStep-1,NumObj);
		ValidTrackStep
		for k= 1 : NumObj
			ThisRealTrace	=   cell2mat(RealTracks(:,k));
			ThisMLETrace	=   cell2mat(MLETracks(:,k));
			plot(ThisRealTrace(:,1),ThisRealTrace(:,2),'b-','linewidth',3,'markers',5);
			plot(ThisMLETrace(:,1),ThisMLETrace(:,2),'r--','linewidth',3,'markers',5);
			if ValidTrackStep >= 2
				RealVelocity(:,k)	=   sqrt(sum((ThisRealTrace(2:end,:)-ThisRealTrace(1:end-1,:)).^2,2))./deltaRange;
				MLEVelocity(:,k)	=   sqrt(sum((ThisMLETrace(2:end,:)-ThisMLETrace(1:end-1,:)).^2,2))./deltaRange;
			end
		end
		legend('Real track','Estiamted track');
		axis equal;
		axis ([0 XYRegion(1) 0 XYRegion(2)]);
		title('Realtrack vs estimated track');
		xlabel('x(m)');
		ylabel('y(m)');
		grid on;
		set(gca,'FontSize',15);
		
		figure (h4)
		subplot(4,1,4)
		for k=1: NumObj
			plot([1:ValidTrackStep-1]',RealVelocity(:,k),'b-');
			plot([1:ValidTrackStep-1]',MLEVelocity(:,k),'r-');
			if k==1
				legend('Real Velocity','Est Velocity');
			end
		end
		title('Real velocity vs Estimated velocity');
		figure (h6)
		cdfplot(CumulatedError(1:CumulatedErrorCount));
		xlabel('Location error(m)');
		ylabel('ratio');
	end
	%After process
	if UsingPresenceSensor
		LastPSensorState = PSensorState;
		LastPatchOccupied = PatchOccupied;
	end
	if UsingRangeSensor
		if ~isempty(find(FollowState==0))
			LastLocated=0;
		end
	end
end
[F2,X2 ] = ecdf(CDFError(1 : UpdatedCount));
figure(TraceHandler)
clf();
hold on;
for i = 1 : NumObj
	plot(TrackX(1:count-1,i),TrackY(1:count-1,i),ObjColor{i});
	plot(EstTrackX(1 : UpdatedCount,i),EstTrackY(1: UpdatedCount ,i),[ObjColor{i},'--']);
end
sdf('pdf');
saveas(gcf,'5traces.pdf','pdf');

figure(AllPatchHandler);
clf();
hold on;
%ColorThePatch(AllPatchHandler,PatchSet,PatchesColorMat,BandColor,PatchCenter);
ColorThePatch(h,PatchSet,PatchesColorMat,ObjColor,BandColor,PatchCenter,ArcSet,XYPSensor,PSensorRadius);
figure();
plot(F1,X1);
%pause();
