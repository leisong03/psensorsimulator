function [Count Patch] = UpperMostPatch(PatchOccupied, PatchGraph,PSensorState, PatchIdx)
Count = 0;
Patch = [ ];
CumulatedIdx = [ ];
while ~isempty(find(PatchOccupied>0));
    OnPatchSet = find(PatchOccupied>0);
    OnPatchIdx = PatchIdx(OnPatchSet,:);
    nnzOnRow = sum(OnPatchIdx, 2);
    MaxIdx = OnPatchSet(find(nnzOnRow == max(nnzOnRow),1,'first'));
    CumulatedIdx = [PatchIdx(MaxIdx,:) CumulatedIdx];
    CumulatedIdx = unique(CumulatedIdx);
    for i = OnPatchSet
	if all(ismember(PatchIdx(i,:),CumulatedIdx))
	    PatchOccupied(i) =  0;
	end
    end
    Patch = [Patch, MaxIdx];
end
ClearPatch = [];
for  i = 1 : length(Patch)
    Rest = Patch([1:i-1,i+1:end]);
    Rest = unique(PatchIdx(Rest,:));
    if ~all(ismember(unique(PatchIdx(i,:)),Rest))
	ClearPatch = [ClearPatch, Patch(i)];
    end
end
Patch = Patch;
Count = length(Patch);
end
