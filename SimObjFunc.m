function [CumulatedError]= SimObjFunc(omega);
close all;
%%%%%%%%%%%%%%%%%%%%%%%%%% Configure moduel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
UsingPresenceSensor=0; 
UsingRangeSensor=1; 
UsingMicrowaveSensor=0; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%% Objects and Region %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NumObj=10; % number of objects 
NumParticleLimit =  20; % number of maximum particle
PSensorRadius = 2; % sensing radius of presence sensor
MSensorRadius = 1; % sensing radius of Motion sensor
XYRegion    =	[10,10]; % size of the reogion

AgentVel=1;
delta=0.01;
T=[0:delta:20];
XYObj=repmat(XYRegion,NumObj,1).* rand(NumObj,2); %size of the 
AgentVel=repmat(AgentVel,NumObj,1);
MaxVelocity=max(AgentVel);

%%%%%%%%%%%%%%%%%%%%%%%% using presence sensor%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if UsingPresenceSensor
%NumPSensor=100; % number of presence sensor
% distributed uniformlly
    [tx ty]=meshgrid([0 :2*sqrt(2) : XYRegion(1)],[0 :2*sqrt(2) :XYRegion(2)]);
    XYPSensor=[tx(:),ty(:)];
    NumPSensor  =	size(XYPSensor,1); % number of presence sensor
    PSensorMat  =	AdjacentMatrix(XYPSensor,PSensorRadius);
    PSensorShrinkSegment	=   cell(NumPSensor,1);
    PSensorExpandSegment	=   cell(NumPSensor,2); 

    %XYPSensor=repmat(XYRegion,NumPSensor,1).* rand(NumPSensor,2); %size of the 
    PSensorState	    =	zeros(1,NumPSensor); % state of Presence sensor
    PSensorStateHistory =	zeros(NumPSensor,10); % state of Presence sensor in last short while
    PSensorOnSegment    =	cell(NumPSensor,1);
    for i=1 : NumPSensor
	PSensorOnSegment{i}=[]; % Store the segment of the Psensor which is triggered on 
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%% using microwave sensor $%%%%%%%%%%%%%%%%%%%%%%%%

if UsingMicrowaveSensor
    NumMSensor=10; 
    XYMSensor=repmat(XYRegion,NumMSensor,1).* rand(NumMSensor,2); %size of the 
    MSensorInWave=ones(1,NumMSensor)*1000;
    MSensorOutWave=ones(1,NumMSensor)*1000;
    DangerMat=sparse(length([0:5:XYRegion(1)]),length([0:5:XYRegion(2)]));
    DangerMat=rand(length([0:5:XYRegion(1)]),length([0:5:XYRegion(2)]))>.7;
    LastPSensorState=zeros(1,NumPSensor);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Using Range sensor %%%%%%%%%%%%%%%%%%%%% 
if UsingRangeSensor
    %NumRangeSensor=10;		    %number of range sensor
    RangeSensorRadius=5;	    % sensing radius of range sensor
    LengthAfterShock=omega*330;	    % length of aftershock
    [tx ty]=meshgrid([-5 :2 : XYRegion(1)+5],[-5 :2 :XYRegion(2)+5]); % Sensor is deployed uniformlly on X-Y
    XYRangeSensor=[tx(:),ty(:)];
    NumRangeSensor=size(XYRangeSensor,1);
    LastLocated=0;		    % to determine if locate success in last time
    LastLocationEst=zeros(NumObj,2);	%Estimated location in last time
    VelocitySample=cell(NumObj,1);  %Historical velocity estimation 
    IntervalCount=5;		    % Ranging is carried out every 5 tic-toc
    deltaRange=delta*IntervalCount; % Time interval of two ranging 
    RealRange=sparse(NumRangeSensor,NumObj); % Store the real range between emitter to  sensor 
    TotalParticle=cell(NumObj,1);   % Save all particle 
    MaxStepInTrack=100;		    % number of steps in track 
    MLETracks	=   cell(MaxStepInTrack,NumObj);  %Track with maximum likelihood  
    RealTracks	=   cell(MaxStepInTrack,NumObj);  %RealTracks 
    ValidTrackStep =0;		    %number of valid step in each track
    CumulatedError=ones(1,length(T))*inf;
    CumulatedErrorCount=0;
    FollowState=ones(1,NumObj);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if UsingPresenceSensor
    h=figure();
    h1=figure();
    h2=figure();
end
if UsingRangeSensor
    h3=figure();
    movegui(h3,'northwest');
    h4=figure();
    movegui(h4,'north');
    h5=figure();
    movegui(h5,'west');
    h6=figure();
    movegui(h6,'center');
end

ttmemo=[];
PrintCount=0;
for i=1:length(T);
    t=T(i);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Generate track of object %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if mod(i,100)==1 % every 5 seconds, change the direction of agent
	tdir=2*pi*rand(NumObj,1);
    end
    [DeltaX, DeltaY]=pol2cart(tdir,AgentVel*delta);
    XYOffset=[DeltaX, DeltaY];
    XYObj=XYObj+XYOffset;
    [XYObj,tdir]=ReflectProcess(XYObj,XYRegion,tdir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Simulation Respoence of Presence sensor %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if UsingPresenceSensor
    for k=1: NumPSensor
	PSensorOnSegment{k}=[];
	PSensorState(k)=0;
	for j=1:NumObj
	    if norm(XYObj(j,:)-XYPSensor(k,:))<PSensorRadius;
		PSensorOnSegment{k}=[0,2*pi];
		PSensorState(k)=1;
	    end
	end
    end
    PSensorStateHistory=[PSensorState' PSensorStateHistory(:,1:end-1)];
    if PSensorState==LastPSensorState  
	disp('same');
	LastPSensorState=PSensorState;
	continue;
    end
    if LastPSensorState==zeros(1,NumPSensor)
	disp('first');
	LastPSensorState=PSensorState;
	continue;
    end
    PrintCount=PrintCount+1;
    AfterName=sprintf('%03d',PrintCount);
    [PSensorShrinkSegment,PSensorExpandSegment,InMove,OutMove]=DiffEdge(PSensorState,LastPSensorState,XYPSensor,PSensorRadius,PSensorMat);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Range sesnor %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
if UsingRangeSensor
    if mod(i,IntervalCount)~=0
	disp('skip');
	continue;
    end
    disp('Ok');
    for j=1: NumRangeSensor
	for k=1:NumObj
	    RealRange(j,k)=norm(XYRangeSensor(j,:)-XYObj(k,:));
	end
    end
    ValidTrackStep=ValidTrackStep+1;
    for k=1 : NumObj
	RealTracks(ValidTrackStep,k)={XYObj(k,:)};
    end
    idx=find(full(RealRange)>RangeSensorRadius); % Range lost when exceed range sensor
    RealRange(idx)=0;

    RealRangeNumMat=full(RealRange)>0;
    ObRange = sparse(NumRangeSensor,NumObj);
    ObRange = AnonymousProcess(RealRange,LengthAfterShock);% Considering aftershock effect
    EstError=zeros(1,NumObj);
    MeanVel=zeros(NumObj,1);
    StdVel=zeros(NumObj,1);
    if LastLocated==0 %If last location is not success, i.e., the historical information is not available 
	GuessedRangeNumMat=RealRangeNumMat;
	XYObjEst = ExpliciteLocate(RealRange,XYRangeSensor,NumObj);
	LastLocationEst=XYObjEst;
	for k=1 : NumObj
	    TotalParticle{k} = {XYObjEst(k,:);XYObjEst(k,:)};
	    MLETracks(ValidTrackStep,k)={XYObj(k,:)};
	end
	LastLocated=1;
	FollowState=ones(1,NumObj);
    else % otherwise 
	[XYObjEstCell,STATE,Residule,GuessedRangeNumMat]=AnonymousLocate(ObRange,XYRangeSensor,NumObj,LastLocationEst,MaxVelocity,deltaRange);
	for k=1 : NumObj
	    [XYObjEstMLE,TotalParticle{k},VelocitySample{k},isFollow]=ParticleFilter(TotalParticle{k},XYObjEstCell{k},Residule{k},NumParticleLimit,VelocitySample{k},deltaRange);
	    if isFollow==0
		disp(['particle ',num2str(k),' lost']);
		FollowState(k)=0;
	    end
	    XYObjEst(k,:) = XYObjEstMLE;
	    EstError(k)=norm(XYObjEstMLE-XYObj(k,:));
	    MLETracks(ValidTrackStep,k)={XYObjEst(k,:)};
	end
	for i=1 : NumObj
	    MeanVel(i)=mean(VelocitySample{k});
	    StdVel(i)=std(VelocitySample{k});
	end
	LastLocationEst=XYObjEst;
    end
    for k=1 : NumObj
    end
    disp('RealRangeNum Observed GuessedRangeNum RangeLoss');
    sumofsum=[sum(RealRangeNumMat)',sum(full(ObRange)>0)', sum(GuessedRangeNumMat)',sum(RealRangeNumMat)'-sum(GuessedRangeNumMat)']
    CumulatedError(CumulatedErrorCount+1:CumulatedErrorCount+length(EstError))=EstError;
    CumulatedErrorCount=CumulatedErrorCount+length(EstError);
    if ValidTrackStep>MaxStepInTrack
	%trim is over-length
	RealTracks=RealTracks(2:end,:);
	MLETracks=MLETracks(2:end,:);
	ValidTrackStep=MaxStepInTrack;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rendering  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
if UsingPresenceSensor
    figure(h);
    clf();
    title(['time=',num2str(t)]);
    hold on;
    axis equal;
    axis ([0, XYRegion(1),0, XYRegion(2)]);
    UpdateXYObj(h,XYObj,XYRegion);
    PSensorOnSegment=UpdateArc(XYPSensor,PSensorMat,PSensorState,PSensorRadius,PSensorOnSegment);
    UpdatePSensor(h,NumPSensor,PSensorRadius,XYPSensor,PSensorOnSegment,PSensorState,PSensorExpandSegment,PSensorShrinkSegment);
    UpdateMove(h,InMove,OutMove,XYPSensor,PSensorRadius);
    print(h,'-dpdf',['./fig/Arc_',AfterName,'.pdf']);

    figure(h1);
    clf();
    title(['time=',num2str(t)]);
    hold on;
    axis equal;
    axis ([0, XYRegion(1),0, XYRegion(2)]);
    UpdatePSensor(h1,NumPSensor,PSensorRadius,XYPSensor,PSensorOnSegment,PSensorState,PSensorExpandSegment,PSensorShrinkSegment);
    print(h1,'-dpdf',['./fig/Q_',AfterName,'.pdf']);

    figure(h2);
    clf();
    hold on
    title(['time=',num2str(t)]);
    idx=find(PSensorState);
    gplot(PSensorMat(idx,idx),XYPSensor(idx,:));
    axis equal;
    axis ([-5, XYRegion(1),-5, XYRegion(2)]);
    plot(XYPSensor(idx,1),XYPSensor(idx,2),'bd');

    print(h2,'-dpdf',['./fig/gplot_',AfterName,'.pdf']);
    [S,C]=graphconncomp(sparse(PSensorMat(idx,idx)));
    %set(gca,'YTick',[0,1])
    pause(0);
end

if UsingRangeSensor
    figure(h3);
    %alwaysontodf(h3);
    clf();
    hold on;
    axis equal;
    axis ([0 XYRegion(1) 0 XYRegion(2)]);
    plot(XYRangeSensor(:,1),XYRangeSensor(:,2),'kd','markers',12,'linewidth',2);
    UpdateXYObj(h3,XYObj,XYRegion,10,2);
    DrawXYObj(h3,NumObj,XYObjEst,'ro',12,2);
    IdxMaxError=find( EstError==max(EstError),1,'first');
    TotalParticle{IdxMaxError};
    AllSpot=cell2mat(TotalParticle{IdxMaxError});
    AllSpotLastLine=AllSpot(end,:);
    %plot(AllSpotLastLine(1:2:end),AllSpotLastLine(2:2:end),'ro','markers',12,'linewidth',3);
    legend('Receiver','Real obj','Est obj');
    set(gca,'FontSize',15);
    %DrawRange(h3,XYObj,XYRangeSensor,RealRangeNumMat-GuessedRangeNumMat);

    figure (h4);
    hold on;
    %alwaysontop(h4);
    subplot(4,1,1)
    hold on;
    idx=find(FollowState);
    bar(idx,EstError(idx));
    idx=find(~FollowState);
    if ~isempty(idx)
	bar(idx,EstError(idx),'r');
    end
    title('locating error');
    xlabel('Identity of target');
    ylabel('Location error(m)');
    subplot(4,1,2)
    bar(sumofsum(:,3));
    title('Number of feasible range');
    xlabel('Identity of target');
    ylabel('Number');
    subplot(4,1,3)
    errorbar([1:NumObj],MeanVel,StdVel);
    set(gca,'FontSize',15);
    title('Esimated velocity');
    xlabel('Identity of target');
    ylabel('Velocity (m/s)');
    %pause();
    
    figure (h5);
    %alwaysontop(h5);
    clf();
    hold on;
    RealVelocity=zeros(ValidTrackStep-1,NumObj);
    MLEVelocity=zeros(ValidTrackStep-1,NumObj);
    ValidTrackStep 
    for k= 1 : NumObj
	ThisRealTrace	=   cell2mat(RealTracks(:,k));
	ThisMLETrace	=   cell2mat(MLETracks(:,k));
	plot(ThisRealTrace(:,1),ThisRealTrace(:,2),'b-','linewidth',3,'markers',5);
	plot(ThisMLETrace(:,1),ThisMLETrace(:,2),'r--','linewidth',3,'markers',5);
	if ValidTrackStep >= 2
	    RealVelocity(:,k)	=   sqrt(sum((ThisRealTrace(2:end,:)-ThisRealTrace(1:end-1,:)).^2,2))./deltaRange;
	    MLEVelocity(:,k)	=   sqrt(sum((ThisMLETrace(2:end,:)-ThisMLETrace(1:end-1,:)).^2,2))./deltaRange;
	end
    end
    legend('Real track','Estiamted track');
    axis equal;
    axis ([0 XYRegion(1) 0 XYRegion(2)]);
    title('Realtrack vs estimated track');
    xlabel('x(m)');
    ylabel('y(m)');
    grid on;
    set(gca,'FontSize',15);

    figure (h4)
    subplot(4,1,4)
    for k=1: NumObj
	plot([1:ValidTrackStep-1]',RealVelocity(:,k),'b-');
	plot([1:ValidTrackStep-1]',MLEVelocity(:,k),'r-');
	if k==1
	    legend('Real Velocity','Est Velocity');
	end
    end
    title('Real velocity vs Estimated velocity');
    figure (h6)
    cdfplot(CumulatedError(1:CumulatedErrorCount));
    xlabel('Location error(m)');
    ylabel('ratio');
end
%After process
if UsingPresenceSensor
    LastPSensorState=PSensrState;
end
if UsingRangeSensor 
    if ~isempty(find(FollowState==0))
	LastLocated=0;
    end
end
end
