t1 = [1 1 2 2 3 3 4 4 5 5 5 6 6 6 6 8 8];
t2 = [4 4 4 4 5 5 5 5 5 5 6 6 6 6 6 6 8]
t3 = [5 5 5 5 5 5 5 5 5 6 6 6 6 6 8 8 8]
t4 = [5 5 5 5 5 5 5 5 6 6 6 6 6 6 8 8 8]
t5 = [5 5 5 5 5 5 5 5 5 5 5 5 6 6 8 8 8]
figure();
boxplot([t1',t2',t3',t4',t5']);
sdf('pdf');
xlabel('Step in MCPF');
ylabel('Number of targets over  particles')
saveas(gcf,'boxfigure.pdf','pdf')