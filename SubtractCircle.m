function OnSegment=SubtractCircle(OnSegment,froma, toa)
NeedToSubtract=[];
if froma>toa
    NeedToSubtract=[froma, 2*pi;0,toa];
else
    NeedToSubtract=[froma,toa];
end
for i=1:size(NeedToSubtract,1)
    j=1;
    while j<= size(OnSegment,1)
	OnSegmentT=OnSegment(j,:);
	NeedToSubtractT=NeedToSubtract(i,:);
	if NeedToSubtract(i,2)<=OnSegment(j,1)||OnSegment(j,2)<=NeedToSubtract(i,1)
	    j=j+1;
	    continue;
	end
	left=( OnSegment(j,1)<NeedToSubtract(i,1));
	right=( OnSegment(j,2)<NeedToSubtract(i,2));

	state=[left,right];
	switch left*10+right
	    case 00
		OnSegment(j,:)=[NeedToSubtract(i,2),OnSegment(j,2)];
		j=j+1;
	    case 01
		OnSegment(j,:)=[];
	    case 10
		OnSegment(end+1,:)=[NeedToSubtract(i,2),OnSegment(j,2)];
		OnSegment(j,:)=[OnSegment(j,1),NeedToSubtract(i,1)];
		j=j+1;
	    case 11
		OnSegment(j,:)=[OnSegment(j,1),NeedToSubtract(i,1)];
		j=j+1;
	end
    end
end
end
