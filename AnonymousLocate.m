function [XYObjEst,STATE,Residule,GuessedRangeNum] = AnonymousLocate(ObRange,XYRangeSensor,NumObj,LastLocationEst,MaxVelocity,deltaRange);
NumRangeSensor=size(XYRangeSensor,1);
debug=0;
% Parameters:
% ObRange - observed range reported by range sensor, which has been anonymous processed
% XYRangeSensor - Location of range sensor 
% NumObj - number of objects
% LastLocationEst - last location estimation of each object
% MaxVelocity - upper bound of objects' velocity 
% deltaRange - time interval between two successive ranging 

% Return
% XYObjEst - Estimation to Objects' location is 2D 
% Residule - AX-b
% GuessedRangeNum - is denoted by [a_ij], where a_ij refers to the number of range reported by i
%		    have been assigned to object j

XYObjEst = cell(NumObj,1);
STATE    = cell(NumObj,1);
Residule = cell(NumObj,1);
FeasibleRange=cell(NumRangeSensor,NumObj); %init a cell, which will hold the feasible range

NumFeasibleRangeSize=zeros(NumObj,1);
MaxRangeNum=zeros(NumObj,1);
GuessedRangeNum=zeros(NumRangeSensor,NumObj);


for i=1 : NumObj
    % for object i
    XY=LastLocationEst(i,:);

    for j=1 : NumRangeSensor
	dij=norm(XY-XYRangeSensor(j,:)); % distance between range sensor j and object i's hist location
	offset=1.2*deltaRange*MaxVelocity; % radius of feasible circle
	ObRangeToThisRangeSensor=ObRange(j,:); % get j row of observed range
	idx=find(dij-offset<=ObRangeToThisRangeSensor & ObRangeToThisRangeSensor<=dij+offset); % find non-zero element in j rom of obrange
	ObRangeToThisRangeSensor = ObRangeToThisRangeSensor(idx);  
	FeasibleRange{j,i}=[ObRangeToThisRangeSensor]; %feasible range between object i and range sensor j
    end
    FeasibleRangeSize=cellfun(@length,FeasibleRange(:,i));
    GuessedRangeNum(:,i)=FeasibleRangeSize;
    NumFeasibleRangeSize(i)=sum(FeasibleRangeSize);
    MaxRangeNum(i)=max(FeasibleRangeSize);
    NumValidRangeSensor=sum(FeasibleRangeSize>0);
    if NumValidRangeSensor>=4
	CombinationSize=4;
    elseif NumValidRangeSensor==3
	CombinationSize=3;
    else
	STATE{i}=[];
	continue;
    end
    tx=find(FeasibleRangeSize);% idx of valid sensor
    IdxCombination=nchoosek(tx,CombinationSize); % choose CombinationSize different Range Sensor
    NumSensorCombination=size(IdxCombination,1);
    if debug
	disp(['Object ',num2str(i),' generate ',num2str(NumSensorCombination), ' combination of size', num2str(CombinationSize),' from ',num2str(NumValidRangeSensor)]);
    end
    PotentialNumRangeCombination=NumSensorCombination*(2^CombinationSize);
    XYObjEst{i} = zeros(PotentialNumRangeCombination,2);
    STATE{i}    = zeros(PotentialNumRangeCombination,1);
    Residule{i} = ones(PotentialNumRangeCombination,1);

    %generate range combination 
    RangeCombCount=0;
    for ThisRow=1 : size(IdxCombination,1)
	idx=IdxCombination(ThisRow,:);
	% generate range combination 
	if CombinationSize==4
	    [x y z r]=ndgrid(FeasibleRange{[idx],i});
	    ThisCombineFeasibleRange=[x(:) y(:) z(:) r(:)];
	end
	if CombinationSize==3
	    [x y z]=ndgrid(FeasibleRange{[idx],i});
	    ThisCombineFeasibleRange=[x(:) y(:) z(:)];
	end
	numcombine  = size(ThisCombineFeasibleRange,1);
	for s=1 : numcombine
	    D=ThisCombineFeasibleRange(s,:);
	    [ThisXY,FLAG,RELRES]=trigulation(XYRangeSensor(idx,:),D');
	    if FLAG==-10
		continue;
	    end
	    RangeCombCount=RangeCombCount+1;
	    XYObjEst{i}(RangeCombCount,:)=ThisXY';
	    STATE{i}(RangeCombCount)=FLAG;
	    Residule{i}(RangeCombCount)=RELRES;
	end
    end
    XYObjEst{i} = XYObjEst{i}([1: RangeCombCount],:);
    STATE{i}    = STATE{i}([1 : RangeCombCount],:);
    Residule{i} = Residule{i}([1 : RangeCombCount],:); 
end
%disp(['NumGuessRange ', 'MaxRangerSensor ' ]);
[NumFeasibleRangeSize MaxRangeNum ];
end
