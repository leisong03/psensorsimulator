function [ MinDist ] = MinDistFCA( FCA_A,FCA_B,PSensorRadius,XY_A,XY_B)
    %MinDist = MINDISTFCA(FCA_A,FCA_B) calculate the minimum distance between two FCA set
    %   FCA_A, and FCA_B is two FCA sets 
    %   MinDist is the distance from FCA_A to FCA_B
    MinDist = inf;
    if numel(FCA_A) == 0 || numel(FCA_B) ==0
	MinDist = 0;
	return;
    end
    for i = 1 : size(FCA_A,1);
	for j = 1 : size(FCA_B,1);
	    ArcPointA = SampleFCA(FCA_A(i,:),PSensorRadius,XY_A)
	    ArcPointB = SampleFCA(FCA_B(j,:),PSensorRadius,XY_B)
	    DistMat = pdist2(ArcPointA, ArcPointB);
	    ThisMinDist = min(DistMat(:));
	    if ThisMinDist < MinDist
		MinDist = ThisMinDist;
	    end
	end
    end

end

function ArcPoint = SampleFCA(FCA,PSensorRadius,XY)
    ExpandSegmentSample=[];
    if FCA(1)>FCA(2);
	ExpandSegmentSample=linspace(FCA(1),2*pi,10);
	ExpandSegmentSample=[ExpandSegmentSample,linspace(0,FCA(2),10)];
    else
	ExpandSegmentSample=linspace(FCA(1),FCA(2),20);
    end
    ArcPoint = [cos(ExpandSegmentSample')*PSensorRadius+XY(1),sin(ExpandSegmentSample')*PSensorRadius+XY(2)];
end
