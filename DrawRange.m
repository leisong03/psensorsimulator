function  DrawRange(h3,XYObj,XYRangeSensor,RangeNumMat);
NumObj=size(XYObj,1);
NumRangeSensor=size(XYRangeSensor,1);
figure(h3);
for i=1 : NumObj
    for j=1 : NumRangeSensor
	if RangeNumMat(j,i) ;
	    line([XYRangeSensor(j,1);XYObj(i,1)],[XYRangeSensor(j,2);XYObj(i,2)]);
	end
    end
end
