function [X,Y] = PatchProfile(PatchID, PatchSet, ArcSet, XYPSensor, PSensorRadius)
ThisPatch = PatchSet{PatchID};
Sspot = [];
Espot = [];
Mspot = [];
for i =  PatchSet{PatchID}'
    PSensorID= i(1); 
    ArcID = i(2);
    [thisX,thisY] = ArcIdx2XY(i(1),i(2), ArcSet, XYPSensor, PSensorRadius);
    Sspot = [Sspot; [thisX(1), thisY(1)]];
    Espot = [Espot; [thisX(2), thisY(2)]];
    Mspot = [Mspot; [thisX(3), thisY(3)]];
end
tx = [Sspot(:,1);Espot(:,1)];
ty = [Sspot(:,2);Espot(:,2)];
meanx = mean(tx);
meany = mean(ty);
ShiftSspot = Sspot - repmat([meanx,meany],size(Sspot,1),1); 
ShiftEspot = Espot - repmat([meanx,meany],size(Espot,1),1);
ShiftMspot = Mspot - repmat([meanx,meany],size(Mspot,1),1);

[AngleSspot,r] = cart2pol(ShiftSspot(:,1),ShiftSspot(:,2));
[AngleEspot,r] = cart2pol(ShiftEspot(:,1),ShiftEspot(:,2));
[AngleMspot,r] = cart2pol(ShiftMspot(:,1),ShiftMspot(:,2));
AngleSspot = wrapTo2Pi(AngleSspot);
AngleEspot = wrapTo2Pi(AngleEspot);
AngleMspot = wrapTo2Pi(AngleMspot);
invalididx = find(AngleSspot > AngleEspot);
AngleEspot(invalididx) = AngleEspot(invalididx) + 2*pi;
X = [];
Y = [];
[val, idx ] = sort(AngleMspot);
Order = [];
for i = idx'
    PSensorID =  ThisPatch(i,1);
    ArcID =  ThisPatch(i,2);
    norm(XYPSensor(PSensorID,:) - [meanx, meany]);
    ThisArcAngle = [];
    if norm(XYPSensor(PSensorID,:) - [meanx, meany]) <=  PSensorRadius
	if ArcSet{PSensorID}(ArcID,1) > ArcSet{PSensorID}(ArcID,2)
	    ThisArcAngle = linspace(ArcSet{PSensorID}(ArcID,1), ArcSet{PSensorID}(ArcID,2)+2*pi, 10);
	else
	    ThisArcAngle = linspace(ArcSet{PSensorID}(ArcID,1), ArcSet{PSensorID}(ArcID,2), 10);
	end
	Order = [Order; 2];
    else
	if ArcSet{PSensorID}(ArcID,1) > ArcSet{PSensorID}(ArcID,2)
	    ThisArcAngle = linspace(ArcSet{PSensorID}(ArcID,2)+2*pi, ArcSet{PSensorID}(ArcID,1), 10);
	else
	    ThisArcAngle = linspace(ArcSet{PSensorID}(ArcID,2), ArcSet{PSensorID}(ArcID,1), 10);
	end
	Order = [Order; 1];
    end
    ThisArcX = cos(ThisArcAngle)*PSensorRadius + XYPSensor(PSensorID,1);
    ThisArcY = sin(ThisArcAngle)*PSensorRadius + XYPSensor(PSensorID,2);
    X = [X, ThisArcX];
    Y = [Y, ThisArcY];
end
%fill(X,Y,'k');
end
