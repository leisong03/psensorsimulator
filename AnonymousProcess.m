function [ObRange] = AnonymousProcess(ObRange, LengthAfterShock )
NumRangeSensor = size(ObRange,1);
NumObj=size(ObRange,2);
for i=1 : NumRangeSensor
    IdxNNZRangeOfThis=find(ObRange(i,:));
    if isempty(IdxNNZRangeOfThis)
	continue;
    end
    % anonymous process
    NNZRangeOfThis=sort(ObRange(i,IdxNNZRangeOfThis));
    % aftershock process 
    ObRange(i,:)=zeros(1,NumObj);
    LossCount=0;
    LastShockFinishAt=0;
    NumValidRange=0;
    for j=1 : length(NNZRangeOfThis)
	if NNZRangeOfThis(j)>LastShockFinishAt;
	    NumValidRange = NumValidRange +1;
	    LastShockFinishAt = NNZRangeOfThis(j)+LengthAfterShock;
	    ObRange(i,NumValidRange) = NNZRangeOfThis(j);
	else
	    LossCount=LossCount+1;
	end
    end
    % disp([num2str(LossCount),'Distances lost from sensor ',num2str(i)]);
    % assmeble into  matrix
    % ObRange(i,[1:length(NNZRangeOfThis)])=NNZRangeOfThis;
end
end
