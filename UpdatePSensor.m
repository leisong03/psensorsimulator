function UpdatePSensor(h,NumPSensor,PSensorRadius,XYPSensor,PSensorOnSegment,PSensorState,PSensorExpandSegment,PSensorShrinkSegment);
for i=1 : NumPSensor
    figure(h);
    OnSegment=PSensorOnSegment{i};
    ExpandSegment=PSensorExpandSegment{i};
    ShrinkSegment=PSensorShrinkSegment{i};
    CircleSample=linspace(0,2*pi,20);
    plot(cos(CircleSample)*PSensorRadius+XYPSensor(i,1),sin(CircleSample)*PSensorRadius+XYPSensor(i,2),'b-','LineStyle',':');
    for j=1:size(OnSegment,1)
	OnSegmentSample=[];
	if OnSegment(j,1)>OnSegment(j,2);
	    OnSegmentSample=linspace(OnSegment(j,1),2*pi,10);
	    OnSegmentSample=[OnSegmentSample,linspace(0,OnSegment(j,2),10)];
	else
	    OnSegmentSample=linspace(OnSegment(j,1),OnSegment(j,2),20);
	end
	plot(cos(OnSegmentSample)*PSensorRadius+XYPSensor(i,1),sin(OnSegmentSample)*PSensorRadius+XYPSensor(i,2),'r-');
    end
    if PSensorState(i)==1
	text(XYPSensor(i,1),XYPSensor(i,2),['S',num2str(i)],'Color','r');
    end
    for j=1:size(ShrinkSegment,1)
	ShrinkSegmentSample=[];
	if ShrinkSegment(j,1)>ShrinkSegment(j,2);
	    ShrinkSegmentSample=linspace(ShrinkSegment(j,1),2*pi,10);
	    ShrinkSegmentSample=[ShrinkSegmentSample,linspace(0,ShrinkSegment(j,2),10)];
	else
	    ShrinkSegmentSample=linspace(ShrinkSegment(j,1),ShrinkSegment(j,2),20);
	end
	plot(cos(ShrinkSegmentSample)*PSensorRadius+XYPSensor(i,1),sin(ShrinkSegmentSample)*PSensorRadius+XYPSensor(i,2),'k-','LineWidth',2);
    end
    for j=1:size(ExpandSegment,1)
	ExpandSegmentSample=[];
	if ExpandSegment(j,1)>ExpandSegment(j,2);
	    ExpandSegmentSample=linspace(ExpandSegment(j,1),2*pi,10);
	    ExpandSegmentSample=[ExpandSegmentSample,linspace(0,ExpandSegment(j,2),10)];
	else
	    ExpandSegmentSample=linspace(ExpandSegment(j,1),ExpandSegment(j,2),20);
	end
	plot(cos(ExpandSegmentSample)*PSensorRadius+XYPSensor(i,1),sin(ExpandSegmentSample)*PSensorRadius+XYPSensor(i,2),'g-','LineWidth',2);
    end
end
end
