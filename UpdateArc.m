function PSensorOnSegment=UpdateArc(XYPSensor, PSensorMat,PSensorState,PSensorRadius,PSensorOnSegment);
    %Draw the edge on figure
    NumPSensor=length(PSensorState);
    idxOn=find(PSensorState~=0);
    idxOff=find(PSensorState==0);
    for i=1: length(idxOn)
	on=idxOn(i);
	for j=1: length(idxOff)
	    off=idxOff(j);
	    if PSensorMat(on,off)
		off2on=XYPSensor(on,:)-XYPSensor(off,:);
		[theta,R]=cart2pol(off2on(1),off2on(2));
		delta=acos(R/(2*PSensorRadius));
		froma=theta-delta;
		toa=theta+delta;
		[froma]=wrapTo2Pi(froma);
		[toa]=wrapTo2Pi(toa);
		PSensorOnSegment{off}=[PSensorOnSegment{off};[froma,toa]];

		on2off=XYPSensor(off,:)-XYPSensor(on,:);
		[theta,R]=cart2pol(on2off(1),on2off(2));
		delta=acos(R/(2*PSensorRadius));
		froma=theta-delta;
		toa=theta+delta;
		[froma]=wrapTo2Pi(froma);
		[toa]=wrapTo2Pi(toa);
		PSensorOnSegment{on}=SubtractCircle(PSensorOnSegment{on},froma,toa);
	    end
	end
    end
    for i=1: length(idxOn)
	for j=1: length(idxOn) 
	    if i==j
		continue
	    end
	    on=idxOn(i);
	    off=idxOn(j);
	    if PSensorMat(on,off)
		off2on=XYPSensor(on,:)-XYPSensor(off,:);
		[theta,R]=cart2pol(off2on(1),off2on(2));
		delta=acos(R/(2*PSensorRadius));
		froma=theta-delta;
		toa=theta+delta;
		[froma]=wrapTo2Pi(froma);
		[toa]=wrapTo2Pi(toa);
		PSensorOnSegment{off}=SubtractCircle(PSensorOnSegment{off},froma,toa);

		on2off=XYPSensor(off,:)-XYPSensor(on,:);
		[theta,R]=cart2pol(on2off(1),on2off(2));
		delta=acos(R/(2*PSensorRadius));
		froma=theta-delta;
		toa=theta+delta;
		[froma]=wrapTo2Pi(froma);
		[toa]=wrapTo2Pi(toa);
		PSensorOnSegment{on}=SubtractCircle(PSensorOnSegment{on},froma,toa);
	    end
	end
    end
end
