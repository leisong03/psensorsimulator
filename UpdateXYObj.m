function UpdateXYObj(h,XYObj,XYRegion);
markers = 4;
linewidth = 2;
NumObj=size(XYObj,1);
figure(h); 
plot(XYObj(:,1),XYObj(:,2),'g*','markers',markers,'linewidth',linewidth);
for i=1:NumObj
    text(XYObj(i,1)+0.2,XYObj(i,2),num2str(i));
end
end
