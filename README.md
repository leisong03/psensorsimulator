# Presence Sensor simulator platform#

This simulator works on simulating the sensing character of Presence sensor(PSensor). As input, motion of object is also provide in this simulator. 
### What is this simulator for? ###

* This simulator is designed to investigate algorithm for PSensor based target tracking algorithm. 
* Algorithm and bound for counting and tracking

### What we achieved on this simulator ###
* We have improve the counting lower bound in 2-dimension case
* We have designed multiple target tracking algorithm 

### Copyright statement ###
* You can use our code anywhere, if our paper citation is included.
```
@inproceedings{Song:2014:MTC:2632951.2632959,
 author = {Song, Lei and Wang, Yongcai},
 title = {Multiple Target Counting and Tracking Using Binary Proximity Sensors: Bounds, Coloring, and Filter},
 booktitle = {Proceedings of the 15th ACM International Symposium on Mobile Ad Hoc Networking and Computing},
 series = {MobiHoc '14},
 year = {2014},
 isbn = {978-1-4503-2620-9},
 location = {Philadelphia, Pennsylvania, USA},
 pages = {397--406},
 numpages = {10},
 url = {http://doi.acm.org/10.1145/2632951.2632959},
 doi = {10.1145/2632951.2632959},
 acmid = {2632959},
 publisher = {ACM},
 address = {New York, NY, USA},
 keywords = {multiple target tracking},
} 
```
