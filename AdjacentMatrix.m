function M=AdjacentMatrix(XYPSensor, PSensorRadius)
    M = pdist2(XYPSensor,XYPSensor);
    M(M<=PSensorRadius*2) = 1;
    M(M>PSensorRadius*2) = 0;
    M(logical(eye(size(M)))) = 0;
end
