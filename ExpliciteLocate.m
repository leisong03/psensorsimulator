function [XYObjEst]=ExpliciteLocate(RealRange,XYRangeSensor,NumObj)
XYObjEst=zeros(NumObj,2);
for i=1:NumObj
    RangeToThisObj=RealRange(:,i);
    Idx=find(RangeToThisObj);
    ValidRange=RangeToThisObj(Idx);
    ValidXYRangeSensor = XYRangeSensor(Idx,:);
    [XY,FLAG,RELRES]=  trigulation(ValidXYRangeSensor,ValidRange);
    XYObjEst(i,:) = XY;
end
end
