function [UpdatedParticle,NumParticles]= UpdateParticle(PatchSet,Dir,FilteredPossibleArc,FilteredPossiblePatch,Particle,NumParticles,UpdateTime,ShiftWindowIdx,MaxNumParticle,PatchGraph,PatchOccupied, Velocity,PatchCenter);

disp(['update particle now, totally ', num2str(NumParticles), ' particles']);
UpdatedParticle = cell(MaxNumParticle*5,1);
UpdatedNumParticle = 0;
ShiftWindowIdx;
DeltaTime = UpdateTime(ShiftWindowIdx) -UpdateTime(ShiftWindowIdx -1);
for i  = 1 : NumParticles
   disp(['There are ',num2str(size(Particle{i},1)),' traces in ',num2str( i ),'st particle ']); 
   for j = 1 : size(Particle{i},1) 
       LastLocation = Particle{i}{j,ShiftWindowIdx - 1};
       disp([num2str(j),'th traces, Last location is ']);
       LastLocation
       disp('match with FilteredPossibleArc');
       IsMatch = 0;
       FilteredPossibleArc
       for k = 1 : size(FilteredPossibleArc,1)
	   if LastLocation(1) ~= 0 % is a arc 
	       if ismember(PatchSet{FilteredPossiblePatch(k)},LastLocation,'rows')
		   MatchOneTrace = 1;
		   UpdatedNumParticle = UpdatedNumParticle + 1;
		   UpdatedParticle{UpdatedNumParticle} =  Particle{i};
		   UpdatedParticle{UpdatedNumParticle}{j,ShiftWindowIdx} = FilteredPossibleArc(k);
	       end
	   else
	       ContainOneArc = 0;
	       for tt = LastLocation(:,2)'
		   if ismember(FilteredPossibleArc(k,:),PatchSet{tt})
		       ContainOneArc = 1; break;
		   end
	       end
	       if ContainOneArc == 1
		   IsMatch = 1; break;
	       end
	   end
       end
       if IsMatch == 1 
	   FilteredPossibleArc(k,:)
	   disp(['Lastlocation ',num2str(LastLocation),' matched with arc ' num2str(FilteredPossibleArc(k,:))]);
	   UpdatedNumParticle = UpdatedNumParticle + 1;
	   UpdatedParticle{UpdatedNumParticle} =  Particle{i};
	   UpdatedParticle{UpdatedNumParticle}{j,ShiftWindowIdx} = FilteredPossibleArc(k,:);
       end
   end
   disp(' ');
end
ThisIdx = find(PatchOccupied > 0);
TrimmedPatchGraph = PatchGraph(ThisIdx,ThisIdx);
for i = 1 : UpdatedNumParticle
    for j = 1 : size(UpdatedParticle{i},1)
	if isempty (UpdatedParticle{i}{j,ShiftWindowIdx})
	    i
	    j
	    ShiftWindowIdx
	    Particle{i}{j,ShiftWindowIdx-1}
	    LastLocation = Particle{i}{j,ShiftWindowIdx-1};
	    if LastLocation(1) ~= 0 % is a arc 
		for k = ThisIdx
		    if ismember(LastLocation,PatchSet{k},'rows') || norm(PatchCenter(LastLocation(1),:) - PatchCenter(k,:)) < DeltaTime*Velocity 
			UpdatedParticle{i}{j,ShiftWindowIdx} = [0,k];
			break;
		    end
		end
	    else
		NewLocation = LastLocation;
		for k =  ThisIdx 
		    if ismember(k, LastLocation(:,2));
			continue;
		    end
		    for q = LastLocation(:,2)'
			if  (full(PatchGraph(k,q)) + full(PatchGraph(q,k))) && norm(PatchCenter(k,:) - PatchCenter(q,:)) < DeltaTime*Velocity
			    NewLocation = [NewLocation; [0,k]];
			    break;
			end
		    end
		end
		UpdatedParticle{i}{j,ShiftWindowIdx} = NewLocation;
	    end
	end
    end
    UpdatedParticle{i}
end
UpdatedParticle =UpdatedParticle(1 : MaxNumParticle); 
NumParticles
end
