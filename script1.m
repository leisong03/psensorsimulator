y=zeros(length([0:0.01:10]),1);
clear;
count=0;
for x=[0:0.1:10];
    count=count+1;
    y(count)=BlindRegion(x,3,0.001,330),
end
func1=@(d) BlindRegion(d,3,0.001,330);
func2=@(d) BlindRegion(d,3,0.002,330);
func3=@(d) BlindRegion(d,3,0.005,330);
func4=@(d) BlindRegion(d,3,0.010,330);
clf();
hold on; 
h1=ezplot(func1,[0,10]);
h2=ezplot(func2,[0,10]);
h3=ezplot(func3,[0,10]);
h4=ezplot(func3,[0,10]);
lw=2;
set(h1,'color','r','linestyle','--','linewidth',lw);
set(h2,'color','g','linestyle','-.','linewidth',lw);
set(h3,'color','b','linestyle',':','linewidth',lw);
set(h4,'color','k','linestyle','-','linewidth',lw);
legend('\omega=1ms','\omega=2ms','\omega=5ms','\omega=10ms');
xlabel('d_{a,b}(m)');
ylabel('S^B_{a\leftarrow b}(m^2)');
sdf('pdf');
title([]);
saveas(gcf,'SBvsD','pdf');
