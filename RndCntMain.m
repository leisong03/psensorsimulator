clear all;
SamplingRate = 10;
NumObj = 30;
MuV = 1;
SigmaV = 0.1;
XBox = 20;
YBox = 20;
TotalSec = 100;
TurnSec = 10;
PSensorRadius = 2;
NumSampe = SamplingRate*TotalSec+1;
XY = zeros(NumSampe,2,NumObj);
XYAOI = [-5 -5; 5 -5; 5 5; -5 5]; 
XYAOI = XYAOI + repmat([XBox YBox]/2,size(XYAOI,1),1);
for i = 1 : NumObj
    [ x,y ] = GiveMeATrace(MuV,SigmaV, SamplingRate, XBox, YBox,TotalSec,TurnSec);
    XY(:,1,i) = x;
    XY(:,2,i) = y;
end
PSensorDensity = .1;
NumPSensor = PSensorDensity*XBox*YBox;
XYPSensor = rand(NumPSensor,2).*repmat([XBox,YBox],NumPSensor,1);
% calculate adjacent matrix 
PSensorAdj = pdist2(XYPSensor,XYPSensor);
PSensorAdj(PSensorAdj > 1.99*PSensorRadius) = 0;
PSensorAdj(PSensorAdj > 0) = 1;
PSensorAdj(logical(eye(size(PSensorAdj)))) = 0;

RealCntV = zeros(NumSampe,1);
SCntV = zeros(NumSampe,1);
DCntV = zeros(NumSampe,1);
TimeSeq = linspace(0,TotalSec,NumSampe);
LastPSensorStatus=zeros(1,NumPSensor);
LastTriggerAt = -inf*ones(1,NumPSensor);
isfirst = 1;
for i = 1 : NumSampe
    isfirst = 0;
    XYSnapshot = zeros(NumObj,2);
    for j = 1 : NumObj
	XYSnapshot(j,:) = XY(i,:,j);
    end
    IsIn = inpolygon( XYSnapshot(:,1),XYSnapshot(:,2),XYAOI(:,1),XYAOI(:,2));

    Obj2SensorD = pdist2(XYSnapshot(logical(IsIn),:),XYPSensor);
    Obj2SensorD(Obj2SensorD > PSensorRadius) = 0;
    Obj2SensorD(Obj2SensorD > 0) = 1;
    PSensorStatus = sum(Obj2SensorD); 
    PSensorStatus(PSensorStatus > 0) = 1;
    LastTriggerAt(PSensorStatus-LastPSensorStatus == 1) = TimeSeq(i);
    
    RealCntV(i) = nnz(IsIn);

    OnPSensorAdj = PSensorAdj(logical(PSensorStatus),logical(PSensorStatus));
    [SCntV(i),Color] = PTASMCP(triu(OnPSensorAdj,1));

    DPSensorAdj = PSensorAdj;
    if ~isfirst
	for j = find(LastTriggerAt == TimeSeq(i))%just triggered sensors
	    Threshold = 0.0001;
	    OnNeighbor = find(PSensorAdj(j,:)&PSensorStatus);
	    for k = OnNeighbor
		if LastTriggerAt(j)-LastTriggerAt(k) < 0.0001; 
		    if rand(1,1)<0.1
			DPSensorAdj(j,k) = 0;
			DPSensorAdj(k,j) = 0;
		    end
		    break;
		end
	    end
	end
    end
    DOnPSensorAdj = DPSensorAdj(logical(PSensorStatus),logical(PSensorStatus));
    [DCntV(i),Color] = PTASMCP(triu(DOnPSensorAdj,1));
end
close all;
plot(TimeSeq, RealCntV,'k-',TimeSeq,SCntV,'b--',TimeSeq,DCntV,'r-.');
sdf('pdf');
xlabel('Time(Sec)');ylabel('Cnt');
legend('Real','Static','Dynamic');
SavePDFto('./CountContrast',gcf );

[FD,XD]= ecdf(RealCntV-DCntV);
[FS,XS]= ecdf(RealCntV-SCntV);
close all;
plot(XS,FS,'b-',XD,FD,'r-');
sdf('pdf');
xlabel('Cnt gap');ylabel('Distribution');
legend('Static','Dynamic');
SavePDFto('./CountCDF',gcf );
