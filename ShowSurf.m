close all;
hold on;
a=[1 2; 3 6; 5 2];
radius=4;
plot(a(:,1),a(:,2),'b*','markers',13);
axis([0 10 -10 10]);
axis equal;
for line=1:size(a,1)
    theta=0:0.1:2*pi;
    x=a(line,1)+cos(theta)*radius;
    y=a(line,2)+sin(theta)*radius;
    plot(x,y,'r-');
end
[x y]=meshgrid([-5:0.1:10],[-5:0.1:15]);
loc=[x(:) y(:)];
count=zeros(size(loc,1),1);
for line=1:size(a,1)
    dist=loc-repmat([a(line,:)],size(loc,1),1);
    dist=dist.^2;
    dist=sqrt(sum(dist,2));
    inidx=find(dist<=radius);
    count(inidx)=count(inidx)+1;
end
countmat=reshape(count,size(x,1),size(x,2));
surf(x,y,countmat)
