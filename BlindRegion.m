function Sb=BlindRegion(d,r,omega,vu)
Sb=0;
if  d>2*r 
    Sb=0;
    return 
elseif d<=2*r && d>=2*r-omega*vu
    theta=acos(d/(2*r));
    Sb= r^2*(theta-sin(theta)*cos(theta));
    return;
elseif d>omega*vu && d<2*r-omega*vu
    theta=acos(d/(2*r));
    bh=d./2;
    ah=vu*omega/2;
    ch=sqrt(ah.^2+bh.^2);
    ybeta=bh/ch*sqrt(r.^2-bh.^2-2*ah*r);
    fc=@(y) 2*sqrt(r.^2-y.^2)-omega*vu*sqrt(1+y.^2/(d.^2-1/4*omega.^2*vu.^2));
    Se=integral(fc,0, ybeta); 
    Sb=r^2*(theta-sin(theta)*cos(theta))-Se;
    return;
elseif d>0 && d<= omega*vu;
    theta=acos(d/(2*r));
    Sb=r^2*(theta-sin(theta)*cos(theta));
    return;
end
