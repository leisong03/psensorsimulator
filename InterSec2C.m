function [fromA,toA] = InterSec2C (fromXY,toXY,PSensorRadius);
    %Calculate the intersection angel of fromXY 
    %fromXY and toXY are center of two circle
    %PSensorRadius is the radius of the circle
    
    vec = toXY-fromXY;
    [Theta, R] = cart2pol(vec(1),vec(2));
    delta = acos(R/(2*PSensorRadius));
    fromA = Theta-delta;
    toA = Theta+delta;
end
