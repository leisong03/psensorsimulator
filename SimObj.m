close all;
T=0:delta:100;
PrintTimeM = zeros(5,length(T)); 
PrintCountV = zeros(5,1);
UpperBoundM = zeros(5,length(T));

Rindex = 0;
for RRratio = [0.5 0.6 0.7 0.8 0.9];
    Rindex = Rindex + 1;

SmartPrint = 1;
NumObj=20; % number of objects 
PSensorRadius=2; % sensing radius of presence sensor
MSensorRadius=1; % sensing radius of Motion sensor
XYRegion=[20,20]; % size of the reogion
AgentVel=1;
Vmax = AgentVel;
delta=0.01;
smallr = RRratio*PSensorRadius;
T=0:delta:100;
XYObj=repmat(XYRegion,NumObj,1).* rand(NumObj,2); %size of the 
if ~SmartPrint
    h=figure('Position',[400,400,740,500]);
end

markers = 4;
linewidth = 2;

LocParam = [2*sqrt(2),0,PSensorRadius;0,0,PSensorRadius];
AreaMat = area_intersect_circle_analytical(LocParam)
UnitIntersectionArea = AreaMat(1,2)
UnitPatchArea = pi*PSensorRadius^2-4*AreaMat(1,2)
UnitArcLength = pi*PSensorRadius/2

% distributed uniformlly
[tx,ty]=meshgrid(0 :2*sqrt(2) : XYRegion(1),0 :2*sqrt(2) :XYRegion(2));
XYPSensor=[tx(:),ty(:)];
NumPSensor=size(XYPSensor,1); 

%find out adjacent relation of Sensors
PSensorMat = pdist2(XYPSensor,XYPSensor);
PSensorMat(PSensorMat<=PSensorRadius*1.99) = 1;
PSensorMat(PSensorMat>PSensorRadius*1.99) = 0;
PSensorMat(logical(eye(size(PSensorMat)))) = 0;

PSensorShrinkSegment=cell(NumPSensor,1);
PSensorExpandSegment=cell(NumPSensor,2);
% number of presence sensor

%XYPSensor=repmat(XYRegion,NumPSensor,1).* rand(NumPSensor,2); %size of the 
PSensorState=zeros(1,NumPSensor); % state of Presence sensor
PSensorStateHistory=zeros(NumPSensor,10);% last pstate sensor
PSensorOnSegment=cell(NumPSensor,1); %Store the segment of Sensor's circle
for i=1 : NumPSensor
    %PSensorOnSegment{i}=rand(2,2).*repmat([2*pi,2*pi],2,1)
    %init the segment set for each psensor
    PSensorOnSegment{i}=[];
end

AgentVel=repmat(AgentVel,NumObj,1);
LastPSensorState=zeros(1,NumPSensor);
PrintCount=0;
StaticCntV = zeros(length(T),1);
UpperBoundV = zeros(length(T),1);
DynamicCntV = zeros(length(T),1);
TriggerOnTime = zeros(NumPSensor,1);
LastFCA = cell(1,NumPSensor);
PrintTime = zeros(length(T),1);
for i=1:length(T);
    t=T(i);
    %XYOffset=random('norm',0,1,NumObj,2).*delta;
    if mod(i,10)==1
	% every 5 seconds, change the direction of agent
	tdir=2*pi*rand(NumObj,1);
    end
    [DeltaX, DeltaY]=pol2cart(tdir,AgentVel*delta);
    XYOffset=[DeltaX, DeltaY];
    XYObj=XYObj+XYOffset;
    [XYObj,tdir]=ReflectProcess(XYObj,XYRegion,tdir);
    for k=1: NumPSensor
	PSensorOnSegment{k}=[];
	PSensorState(k)=0;
	for j=1:NumObj
	    if norm(XYObj(j,:)-XYPSensor(k,:))<PSensorRadius;
		PSensorOnSegment{k}=[0,2*pi];
		PSensorState(k)=1;
	    end
	end
    end
    PSensorStateHistory=[PSensorState' PSensorStateHistory(:,1:end-1)];
    
    if PSensorState==LastPSensorState  
	%if state not change, skip the state estimation
	%disp('same');
	LastPSensorState=PSensorState;
	continue;
    end
    if LastPSensorState==zeros(1,NumPSensor)
	disp('first');
	LastPSensorState=PSensorState;
	continue;
    end

    disp(['time=',num2str(t)]);
    PrintCount=PrintCount+1;
    PrintTime(PrintCount) = t;
    AfterName=sprintf('%03d',PrintCount);

    [PSensorShrinkSegment,PSensorExpandSegment,InMove,OutMove]=DiffEdge(PSensorState,LastPSensorState,XYPSensor,PSensorRadius,PSensorMat);
    % PSensorState and LastPSensorState are both n x 1 vector

    OnIdx = find(PSensorState > 0);
    OnSubMat = PSensorMat(OnIdx, OnIdx); % get the submatrix of on sensor 
    OnSubMat = triu(OnSubMat,1);

    [StaticCnt,Color] = PTASMCP(OnSubMat);  
    StaticCntV(PrintCount) = StaticCnt;
    Num_edge = nnz(triu(OnSubMat,1));
    TotalS = nnz(PSensorState)*UnitPatchArea + Num_edge*UnitIntersectionArea;
    TotalC = nnz(PSensorState)*4*UnitArcLength-Num_edge*2*UnitArcLength;
    UpperBoundV(PrintCount)=(TotalS+smallr*TotalC)/(sqrt(12)*smallr^2);

    AdjustedPSensorMat = PSensorMat;
    if (nnz(PSensorState - LastPSensorState > 0))
	% there are some PSensor is triggered on 
	for j = find(PSensorState - LastPSensorState >0)
	    TriggerOnTime(j) = t; 
	    LastFCA(j) = PSensorShrinkSegment(j);
	    Neighbours = find(PSensorState == 1 & PSensorMat(j,:) == 1 );
	    disp([num2str(j),' is triggered on']);
	    Neighbours
	    NeighboursLastTriggerIn =TriggerOnTime(j)- TriggerOnTime(Neighbours)'
	    NeighboursMaxExpD = NeighboursLastTriggerIn.*Vmax
	    NeighboursMinMeasureD = ones(size(Neighbours)).*-1;
	    NCnt = 0;
	    for k = Neighbours
		NCnt = NCnt + 1;
		DMin = MinDistFCA(LastFCA{k},LastFCA{j},PSensorRadius,XYPSensor(k,:),XYPSensor(j,:));
		TDelta = TriggerOnTime(j)-TriggerOnTime(k);
		NeighboursMinMeasureD(NCnt) = DMin;
		if DMin > TDelta*Vmax
		    disp(['edge deleted beteween ',num2str(j),' and ',num2str(k)]);
		   disp([num2str(j),' s trigger on is:']); 
		   LastFCA{j}
		   disp([num2str(k),' s trigger on is:']); 
		   LastFCA{k}
		  AdjustedPSensorMat(j,k) = 0;  
		  AdjustedPSensorMat(k,j) = 0;  
		end
	    end
	    NeighboursMinMeasureD 
	end
	% Do OnSubMat Adjusted
    end	
    AdjustedOnSubMat = AdjustedPSensorMat(OnIdx, OnIdx); % get the submatrix of on sensor 
    AdjustedOnSubMat = triu(AdjustedOnSubMat,1);

    [DynamicCnt,Color] = PTASMCP(AdjustedOnSubMat); 
    DynamicCntV(PrintCount) = DynamicCnt;
    %Contrast = [StaticCntV(1:PrintCount)  DynamicCntV(1:PrintCount)]


if ~SmartPrint
    figure(h);
    clf();
    subplot(1,2,1)
    hold on;
    axis equal;
    axis ([0, XYRegion(1),0, XYRegion(2)]);
    plot(XYObj(:,1),XYObj(:,2),'g*','markers',markers,'linewidth',linewidth);
    title(['time=',num2str(t)]);
    for i=1:NumObj
	text(XYObj(i,1)+0.2,XYObj(i,2),num2str(i));
    end
end
    PSensorOnSegment=UpdateArc(XYPSensor,PSensorMat,PSensorState,PSensorRadius,PSensorOnSegment);
    if ~SmartPrint
    UpdatePSensor(h,NumPSensor,PSensorRadius,XYPSensor,PSensorOnSegment,PSensorState,PSensorExpandSegment,PSensorShrinkSegment);
    UpdateMove(h,InMove,OutMove,XYPSensor,PSensorRadius);
    %print(h,'-dpdf',['./fig/Arc_',AfterName,'.pdf']);
    xlabel('X(m)'), ylabel('Y(m)');
    box on;
    subplot(1,2,2);
    xlabel('X(m)'), ylabel('Y(m)');
    box on;

    hold on
    title(['time=',num2str(t)]);
    idx=find(PSensorState);
    gplot(PSensorMat(idx,idx),XYPSensor(idx,:));
    plot(XYPSensor(idx,1),XYPSensor(idx,2),'k^');
    %plot(XYPSensor(idx,1),XYPSensor(idx,2),'bd');
    axis equal;
    axis ([0, XYRegion(1),0, XYRegion(2)]);
    fname = ['./fig/ScreenShot_',AfterName];
    sdf('pdf');
    SavePDFto(fname,h);
end
    %[S,C]=graphconncomp(sparse(PSensorMat(idx,idx)));
    LastPSensorState=PSensorState;
    if DynamicCntV(PrintCount) ~= StaticCntV(PrintCount);
	disp('Delete edge detected');
	waitforbuttonpress;
    end
end
PrintTimeM(Rindex,:)=PrintTime;
UpperBoundM(Rindex,:)=UpperBoundV;
PrintCountV(Rindex) = PrintCount;
end
%OriginUpperBoundM = UpperBoundM; 

for i = 1 : 5
    for j = 1 : PrintCountV(i)
	UpperBoundM(i,j) = min(OriginUpperBoundM(i,1:j));
    end
end
plot(PrintTimeM(1,1:PrintCountV(1)),UpperBoundM(1,1:PrintCountV(1)),PrintTimeM(2,1:PrintCountV(2)),UpperBoundM(2,1:PrintCountV(2)),PrintTimeM(3,1:PrintCountV(3)),UpperBoundM(3,1:PrintCountV(3)),PrintTimeM(4,1:PrintCountV(4)),UpperBoundM(4,1:PrintCountV(4)),PrintTimeM(5,1:PrintCountV(5)),UpperBoundM(5,1:PrintCountV(5)),PrintTimeM(1,1:PrintCountV(1)),ones(1,PrintCountV(1))*NumObj);
%for RRratio = [0.5 0.6 0.7 0.8 0.9];
legend('r =0.5R','r =0.6R','r =0.7R','r =0.8R','r =0.9R','Ground-truth','Location','Best');
ylim([0,max(max(UpperBoundM))]);
sdf('pdf');
xlabel('time(sec)');ylabel('cnt');
SavePDFto('./fig/UpperBound',gcf);
close all;
