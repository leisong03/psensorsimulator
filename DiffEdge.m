function [PSensorShrinkSegment,PSensorExpandSegment,InMove,OutMove]=DiffEdge(PSensorState,LastPSensorState,XYPSensor,PSensorRadius,PSensorMat)
    % Calculate the edge where moving happens
    
    NumPSensor = length(PSensorState); 
    PSensorShrinkSegment = cell(NumPSensor,1);
    PSensorExpandSegment = cell(NumPSensor,1);

    DiffState = PSensorState-LastPSensorState;% state difference
    InMove=[]; 
    OutMove=[];

    InIdx = find(DiffState==1); %some object enter this circle
    for ThisInIdx = InIdx
	%ThisInIdxAdj=find(PSensorMat(ThisInIdx,:));% PSensorMat is actually neighboring mat
	ThisInIdxAdjOn=find(PSensorMat(ThisInIdx,:)&PSensorState);
	%ThisInIdxAdjOnLast=find(PSensorMat(ThisInIdx,:)&LastPSensorState);
	for ThisAdjOn=ThisInIdxAdjOn;
	    [fromA,toA]=InterSec2C(XYPSensor(ThisInIdx,:),XYPSensor(ThisAdjOn,:),PSensorRadius);
	    PSensorShrinkSegment{ThisInIdx}=[PSensorShrinkSegment{ThisInIdx};[fromA, toA]];
	    InMove=[InMove;[ThisAdjOn,ThisInIdx]];
	end
    end

    OutIdx=find(DiffState==-1); %some object leave this circle
    for ThisOutIdx=OutIdx
	%ThisOutIdxAdj=find(PSensorMat(ThisOutIdx,:));
	ThisOutIdxAdjOn=find(PSensorMat(ThisOutIdx,:)&PSensorState);
	%ThisOutIdxAdjOnLast=find(PSensorMat(ThisOutIdx,:)&LastPSensorState);
	for ThisAdjOn=ThisOutIdxAdjOn;
	    [fromA,toA]=InterSec2C(XYPSensor(ThisOutIdx,:),XYPSensor(ThisAdjOn,:),PSensorRadius);
	    PSensorExpandSegment{ThisOutIdx}=[PSensorExpandSegment{ThisOutIdx};[fromA, toA]];
	    OutMove=[OutMove;[ThisOutIdx,ThisAdjOn]];
	end
    end
end
