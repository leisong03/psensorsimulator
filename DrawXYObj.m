function DrawXYObj(h,NumObj,XYObj,symbol,markersize,linewidth)
figure(h);
plot(XYObj(:,1),XYObj(:,2),symbol,'markers',markersize,'linewidth',linewidth);
for i=1:NumObj
    text(XYObj(i,1),XYObj(i,2),num2str(i));
end
end
