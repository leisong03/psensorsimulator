function DrawBackground(h,GridX,GridY,XYRegion,DangerMat)
hold on;
figure(h);
for ix=1:length(GridX);
    for iy=1:length(GridY);
	if(DangerMat(ix,iy)==1)
	    fill([GridX(ix),GridX(ix)+5,GridX(ix)+5,GridX(ix)],[GridY(iy),GridY(iy),GridY(iy)+5,GridY(iy)+5],'g');
	end
	x=linspace(GridX(ix),GridX(ix),20);
	y=linspace(0,XYRegion(2),20);
	plot(x,y,'Color','k','LineStyle',':');

	y=linspace(GridY(iy),GridY(iy),20);
	x=linspace(0,XYRegion(1),20);
	plot(x,y,'Color','k','LineStyle',':');
    end
end
end
