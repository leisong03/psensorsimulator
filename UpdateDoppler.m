    clf();
    hold on;
    for k=1: NumMSensor
	subplot(1,NumMSensor,k);
	tsample=[1:0.1:10]
	sample=sin(MSensorInWave(k)*tsample);
	plot(abs(fft(sample)),'r');
	sample=sin(MSensorOutWave(k)*tsample);
	plot(abs(fft(sample)),'g');
	axis([50 100 0 40]);

    end
