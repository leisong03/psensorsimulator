close all;
Plane = figure();
Bar = figure();
OverlapBar = figure();
PlaneSize = [3 3];
gamma = 2;
omega = 330/1000*1;
Interval = [ 1.5 1.5 ];
NumTarget = 3;
XYTarget = rand(NumTarget,2).*repmat([PlaneSize(1),PlaneSize(2)],NumTarget,1);
[X, Y] = meshgrid([0:Interval(1):PlaneSize(1)],[0:Interval(2):PlaneSize(2)]);
XYReceiver  = [X(:),Y(:)];
DistMat = ones(size(XYTarget,1),size(XYReceiver,1))*inf;
for i = 1 : size(DistMat,1)
    for j = 1 : size(DistMat,2)
	if norm(XYTarget(i,:) - XYReceiver(j,:)) < gamma 
	    DistMat(i,j) =  norm(XYTarget(i,:) - XYReceiver(j,:));
	end
    end
end

CleanDistMat = DistMat;
a = find(CleanDistMat == inf);
CleanDistMat(a) = 0;

OverlappedDistMat = DistMat;
for i = 1 : size(DistMat,2)
    for j = 1 : size(DistMat,1)
	if DistMat(j,i) == inf;
	    continue;
	end
	overlapped = find ( DistMat(:,i) < DistMat(j,i) & DistMat(:,i) >= DistMat(j,i)-omega )
	if ~isempty(overlapped)
	    disp(['inrow ',num2str(j),'incolumn ',num2str(i)]);
	    disp('overlap happen');
	    OverlappedDistMat(j,i) = inf;
	end
    end
end
DistMat
CleanDistMat
OverlappedDistMat

figure(Plane);
hold on;
for i = 1 : size(XYTarget,1)
    text(XYTarget(i,1)+0.1,XYTarget(i,2),num2str(i));
end

for i = 1 : size(XYReceiver,1)
    text(XYReceiver(i,1)+0.1,XYReceiver(i,2),num2str(i));
end

plot(XYReceiver(:,1),XYReceiver(:,2),'V');
plot(XYReceiver(:,1),XYReceiver(:,2),'V');
plot(XYTarget(:,1),XYTarget(:,2),'r*');
for i = 1 : size(XYTarget,1)
    circle([XYTarget(i,1),XYTarget(i,2)],gamma,1000,'g--');
end
axis equal;
axis ([-2 PlaneSize(1)+1 -2 PlaneSize(2)+1]);
xlabel('x(m)');
ylabel('y(m)');
box on;
sdf pdf;

saveas(gcf,'4Ranged.pdf','pdf')

index = [1 : size(XYReceiver,1)];
label = {};
for i = 1 : size(XYReceiver,1)
    label{end+1} = ['Rx',num2str(i)];
end
Xlabel = {};
for i = 1 : size(XYTarget,1)
    Xlabel{end+1} = ['Target',num2str(i)];
end

figure(Bar);
bar2(index,CleanDistMat,'BASE','XLABEL',label);
ylabel('range(m)');
%legend('Target1','Target2','Target3');
legend(Xlabel);
box on;
sdf pdf;
saveas(gcf,'BarOnRx.pdf','pdf')

figure(OverlapBar);
bar2(index,OverlappedDistMat,'BASE','XLABEL',label);
ylabel('range(m)');
legend(Xlabel);
box on;
sdf pdf;
saveas(gcf,'OverlapBarOnRx.pdf','pdf')
